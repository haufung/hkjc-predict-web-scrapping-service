from django.db import connection

from ..utils import eloFunctionUtil

cur = connection.cursor()

def updateDeclareWeightIsNull(date, fromRecordsTable, toRecordsTable):
    cur.execute(
        rf'''DROP FUNCTION IF EXISTS calculate_missing_declare_weight(text,date);
    CREATE OR REPLACE FUNCTION calculate_missing_declare_weight (this_horse_id text, this_race_date date)
    RETURNS float AS $$
    declare
    	last_declare_weight float;
    	next_declare_weight float;
    	avg_declare_weight float;
    BEGIN
    	select declare_weight from {fromRecordsTable} where horse_id = this_horse_id and records_date < this_race_date and declare_weight_is_null is not true order by records_date desc limit 1 into last_declare_weight;
    	select declare_weight from {fromRecordsTable} where horse_id = this_horse_id and records_date > this_race_date and declare_weight_is_null is not true order by records_date asc limit 1 into next_declare_weight;
    	select avg(declare_weight::float) from {fromRecordsTable} where declare_weight_is_null is not true into avg_declare_weight;

    	if last_declare_weight is null and next_declare_weight is null then
             RETURN avg_declare_weight;
    	elsif last_declare_weight is null then
    		 RETURN next_declare_weight;
    	elsif next_declare_weight is null then
    		RETURN last_declare_weight;
    	else 
    		RETURN (next_declare_weight + last_declare_weight)/2;
         end if;
    END;
    $$ LANGUAGE plpgsql;''')
    cur.execute(
        rf'''update {toRecordsTable} 
    set declare_weight = alias.estimated_declare_weight
    from (select "calculate_missing_declare_weight"(horse_id,records_date) as estimated_declare_weight, records_id from {toRecordsTable} where declare_weight_is_null is true and {toRecordsTable}.records_date > '{date}') as alias
    where {toRecordsTable}.records_id = alias.records_id;''')


def getDataTypeOfColumn(table, columnName):
    tableNameAndSchema = table.split(".")
    tableName = tableNameAndSchema.pop()
    schema = tableNameAndSchema.pop()
    sqlQuery = rf'''SELECT data_type
  FROM information_schema.columns
 WHERE table_schema = '{schema}'
   AND table_name   = '{tableName}'
   and column_name = '{columnName}'
     ;'''
    cur.execute(sqlQuery)
    return cur.fetchall()[0][0]

def updateIfColumnIsNull(date, toRecordsTable, fieldName):
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName}_is_null boolean;''')
    if getDataTypeOfColumn(toRecordsTable, fieldName) != 'double precision':
        cur.execute(rf'''UPDATE {toRecordsTable} set {fieldName}_is_null = null where {toRecordsTable}.records_date > '{date}' ;''')
        cur.execute(rf'''UPDATE {toRecordsTable} set {fieldName}_is_null = true, {fieldName} = null where {fieldName} like '%-%'  or {fieldName} = '' and {toRecordsTable}.records_date > '{date}' ;''')
        cur.execute(rf'''CREATE INDEX IF NOT EXISTS {fieldName}_is_null_index ON {toRecordsTable} USING btree ({fieldName}_is_null ASC NULLS LAST);''')
    connection.commit()

def updateIfFinishTimeIsZeroOrNull(date, toRecordsTable, fieldName):
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName}_is_null boolean;''')
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName}_is_zero boolean;''')
    cur.execute(rf'''UPDATE {toRecordsTable} set {fieldName}_is_null = null where {toRecordsTable}.records_date > '{date}' ;''')
    cur.execute(rf'''UPDATE {toRecordsTable} set {fieldName}_is_zero = null where {toRecordsTable}.records_date > '{date}' ;''')
    cur.execute(rf'''UPDATE {toRecordsTable} set {fieldName}_is_null = true, {fieldName} = null where {fieldName} = '---' and {toRecordsTable}.records_date > '{date}' ;''')
    cur.execute(rf'''UPDATE {toRecordsTable} set {fieldName}_is_zero = true, {fieldName} = null where {fieldName} = '0:00.00' and {toRecordsTable}.records_date > '{date}' ;''')
    if getDataTypeOfColumn(toRecordsTable, fieldName) != 'time':
        cur.execute(rf'''CREATE INDEX IF NOT EXISTS {fieldName}_is_null_index ON {toRecordsTable} USING btree ({fieldName}_is_null ASC NULLS LAST);''')
        cur.execute(rf'''CREATE INDEX IF NOT EXISTS {fieldName}_is_zero_index ON {toRecordsTable} USING btree ({fieldName}_is_zero ASC NULLS LAST);''')
        cur.execute(rf'''ALTER TABLE {toRecordsTable} ALTER COLUMN finish_time TYPE time USING CASE
                        WHEN {fieldName}='---' THEN null
                        WHEN {fieldName}='0:00.00' THEN null
                        ELSE {fieldName}::time END;''')
    connection.commit()


def updateColumnType(toRecordsTable):
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ALTER COLUMN actual_weight TYPE float USING actual_weight::float;''')
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ALTER COLUMN win_odds TYPE float USING win_odds::float;''')
    connection.commit()
    print("update column type done!")


def updateNullData(date, fromRecordsTable, toRecordsTable):
    updateIfColumnIsNull(date, toRecordsTable, 'draw')
    print("update null draw data done!")
    updateIfColumnIsNull(date, toRecordsTable, 'win_odds')
    print("update null win_odds data done!")
    updateIfColumnIsNull(date, toRecordsTable, 'declare_weight')
    updateDeclareWeightIsNull(date, fromRecordsTable, toRecordsTable)
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ALTER COLUMN declare_weight TYPE float USING declare_weight::float;''')
    cur.execute(rf'''CREATE INDEX IF NOT EXISTS declare_weight_index ON {toRecordsTable} USING btree (declare_weight ASC NULLS LAST);''')
    print("declare_weight_index built!")
    print("update null declare_weight data done!")
    if 'predict.' not in toRecordsTable:
        updateIfFinishTimeIsZeroOrNull(date, toRecordsTable, 'finish_time')
        print("update null or zero finish_time data done!")
    connection.commit()

def buildIndex(raceTable, recordsTable):
    cur.execute(rf'''CREATE INDEX IF NOT EXISTS horse_id_index
    ON {recordsTable} USING btree
    (horse_id ASC NULLS LAST);''')
    cur.execute(rf'''CREATE INDEX IF NOT EXISTS jockey_name_index
        ON {recordsTable} USING btree
        (jockey_name ASC NULLS LAST);''')
    cur.execute(rf'''CREATE INDEX IF NOT EXISTS race_id_index_for_records
        ON {recordsTable} USING btree
        (race_id ASC NULLS LAST);''')
    cur.execute(rf'''CREATE INDEX IF NOT EXISTS records_date_index
        ON {recordsTable} USING btree
        (records_date ASC NULLS LAST);''')
    cur.execute(rf'''CREATE INDEX IF NOT EXISTS records_id_index
        ON {recordsTable} USING btree
        (records_id ASC NULLS LAST);''')
    cur.execute(rf'''CREATE INDEX IF NOT EXISTS trainer_name_index
            ON {recordsTable} USING btree
            (trainer_name ASC NULLS LAST);''')
    cur.execute(rf'''CREATE INDEX IF NOT EXISTS course_index
                ON {raceTable} USING btree
                (course ASC NULLS LAST);''')
    cur.execute(rf'''CREATE INDEX IF NOT EXISTS race_id_index
                ON {raceTable} USING btree
                (race_id ASC NULLS LAST);''')
    connection.commit()
    print("build index done!")

def horseDaysFromFirstRaceUpdate(date, fromRecordsTable, toRecordsTable):
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS horse_days_from_first_race float;''')
    cur.execute(rf'''DROP FUNCTION IF EXISTS horse_days_from_first_race(text,date);
    CREATE OR REPLACE FUNCTION horse_days_from_first_race(this_horse_id text, this_race_date date)
    RETURNS integer AS $$
    declare
    	days_from_first_race integer;
    BEGIN
    select (this_race_date - alias.records_date) as days_from_first_race from
    (select * from {fromRecordsTable}
     where horse_id = this_horse_id order by {fromRecordsTable}.records_date asc limit 1) as alias 
   into days_from_first_race;
   RETURN days_from_first_race;
    END;
        $$ LANGUAGE plpgsql;''')
    cur.execute(rf'''UPDATE {toRecordsTable} 
    SET horse_days_from_first_race = alias.horse_days_from_first_race 
    from (select "horse_days_from_first_race"(horse_id, records_date), records_id from {toRecordsTable}) as alias
    where alias.records_id = {toRecordsTable}.records_id and records_date > '{date}';''')
    connection.commit()
    print("horse_days_from_first_race update completed!")

def daysApartFromPreviousRaceNullCalculation(date, toRecordsTable):
    cur.execute(
        rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS days_apart_from_previous_race_is_null boolean;''')
    cur.execute(
        rf'''UPDATE {toRecordsTable} SET days_apart_from_previous_race_is_null = null where {toRecordsTable}.records_date > '{date}';''')
    cur.execute(
        rf'''UPDATE {toRecordsTable} SET days_apart_from_previous_race_is_null = true where days_apart_from_previous_race is null and {toRecordsTable}.records_date > '{date}';''')
    cur.execute(
        rf'''UPDATE {toRecordsTable} SET days_apart_from_previous_race = (select avg(days_apart_from_previous_race)::integer from model.records where days_apart_from_previous_race_is_null is not true) 
    WHERE days_apart_from_previous_race_is_null is true and {toRecordsTable}.records_date > '{date}';''')
    connection.commit()

def lastPlaceOnSameCourseNullCalculation(date, toRecordsTable):
    for fieldName in ['first', 'second', 'third']:
        cur.execute(
            rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName}_last_place_on_same_course_is_null boolean;''')
        cur.execute(
            rf'''UPDATE {toRecordsTable} SET {fieldName}_last_place_on_same_course_is_null = null where {toRecordsTable}.records_date > '{date}';''')
        cur.execute(
            rf'''UPDATE {toRecordsTable} SET {fieldName}_last_place_on_same_course_is_null = true, {fieldName}_last_place_on_same_course = 'NULL' where {fieldName}_last_place_on_same_course is null and {toRecordsTable}.records_date > '{date}';''')
    connection.commit()

def weightDifferenceFromLastRace(date, toRecordsTable):
    cur.execute(
        rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS weight_difference_from_last_race_is_null boolean;''')
    cur.execute(
        rf'''UPDATE {toRecordsTable} SET weight_difference_from_last_race_is_null = null where {toRecordsTable}.records_date > '{date}';''')
    cur.execute(
        rf'''UPDATE {toRecordsTable} SET weight_difference_from_last_race_is_null = true where weight_difference_from_last_race is null and {toRecordsTable}.records_date > '{date}';''')
    cur.execute(
        rf'''UPDATE {toRecordsTable} SET weight_difference_from_last_race = (select avg(weight_difference_from_last_race)::float from {toRecordsTable} where weight_difference_from_last_race_is_null is not true) 
    WHERE weight_difference_from_last_race_is_null is true and {toRecordsTable}.records_date > '{date}';''')
    connection.commit()

def horseDaysFromFirstRace(date, toRecordsTable):
    cur.execute(
        rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS horse_days_from_first_race_is_null boolean;''')
    cur.execute(
        rf'''UPDATE {toRecordsTable} SET horse_days_from_first_race_is_null = null where {toRecordsTable}.records_date > '{date}';''')
    cur.execute(
        rf'''UPDATE {toRecordsTable} SET horse_days_from_first_race_is_null = true where horse_days_from_first_race is null and {toRecordsTable}.records_date > '{date}';''')
    cur.execute(
        rf'''UPDATE {toRecordsTable} SET horse_days_from_first_race = 0 
    WHERE horse_days_from_first_race_is_null is true and {toRecordsTable}.records_date > '{date}';''')
    connection.commit()

def updateNullCalculation(date, toRecordsTable):
    daysApartFromPreviousRaceNullCalculation(date, toRecordsTable)
    lastPlaceOnSameCourseNullCalculation(date, toRecordsTable)
    weightDifferenceFromLastRace(date, toRecordsTable)
    horseDaysFromFirstRace(date, toRecordsTable)

def dayApartFromPreviousRaceUpdate(date, fromRecordsTable, toRecordsTable, initCalculate):
    if initCalculate is True:
        table = fromRecordsTable
        offset = 1
    else:
        table = toRecordsTable
        offset = 0
    funcQuery = rf'''DROP FUNCTION IF EXISTS days_apart_from_previous_race(text,date);
CREATE OR REPLACE FUNCTION days_apart_from_previous_race (this_horse_id text, this_race_date date )
RETURNS float AS $$
declare
  days_apart float;
BEGIN
   select (alias.records_date - alias2.records_date) as day_apart from
(select * from {table}
 where horse_id = this_horse_id and {table}.records_date <= this_race_date
  order by {table}.records_date desc limit 1) as alias,
(select * from {fromRecordsTable}
 where horse_id = this_horse_id and {fromRecordsTable}.records_date <= this_race_date
  order by {fromRecordsTable}.records_date desc limit 1 offset {offset}) as alias2 
   into days_apart;
   RETURN days_apart;
END;
$$ LANGUAGE plpgsql;'''
    cur.execute(funcQuery)
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS days_apart_from_previous_race float;''')
    sqlQuery = rf'''update {toRecordsTable} 
    set days_apart_from_previous_race = alias.days_apart_from_previous_race
    from (select "days_apart_from_previous_race"(horse_id, records_date) as days_apart_from_previous_race, records_id from {toRecordsTable}) as alias
    where {toRecordsTable}.records_id = alias.records_id and {toRecordsTable}.records_date > '{date}';'''
    cur.execute(sqlQuery)
    print("days_apart_from_previous_race update completed!")
    connection.commit()


def lastPlaceOnSameCourseUpdate(fromRecordsTable, toRecordsTable, fromRaceTable, toRaceTable, firstSecondOrThird, allRace):
    offset = 0
    if firstSecondOrThird == 'first':
        offset = 0
    if firstSecondOrThird == 'second':
        offset = 1
    if firstSecondOrThird == 'third':
        offset = 2
    fieldName = f'{firstSecondOrThird}_last_place_on_same_course'
    funcQuery = rf'''DROP FUNCTION IF EXISTS {fieldName}(text,date,text);
CREATE OR REPLACE FUNCTION {fieldName}(this_horse_id text, this_records_date date , this_race_course text)
RETURNS text AS $$
declare
	{fieldName} text;
BEGIN
   select place from {fromRecordsTable},{fromRaceTable} 
 where horse_id = this_horse_id
 and {fromRecordsTable}.race_id = {fromRaceTable}.race_id 
 and course = this_race_course
 and {fromRecordsTable}.records_date < this_records_date
 order by {fromRecordsTable}.records_date desc limit 1 offset {offset}
   into {fieldName};
   RETURN {fieldName};
END;
$$ LANGUAGE plpgsql;
'''
    cur.execute(funcQuery)
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName} text;''')

    count = 0
    for index, raceId in enumerate(allRace):
        count = count + 1
        updateQuery = rf'''update {toRecordsTable} 
            set {fieldName} = alias.{fieldName}
            from (select "{fieldName}"(horse_id,records_date,course) as {fieldName}, {toRecordsTable}.records_id from {toRaceTable}, {toRecordsTable} 
            where {toRaceTable}.race_id = {toRecordsTable}.race_id and {toRecordsTable}.race_id = '{raceId[0]}') as alias
            where {toRecordsTable}.records_id = alias.records_id;'''
        cur.execute(updateQuery)
        print(f"{fieldName} update of {raceId[0]} completed! Left {len(allRace) - count}")
        connection.commit()

def labelUpdate(date, toRecordsTable):
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS label float;''')
    cur.execute(rf'''update {toRecordsTable} 
    set label = 1
    where (place = '1' or place = '1 DH') and records_date > '{date}';''')
    cur.execute(rf'''update {toRecordsTable} 
        set label = 0
        where (place != '1' and place != '1 DH' and place not in (select abbreviation from config.place_special_incidents_index)) and records_date > '{date}';''')
    cur.execute(rf'''update {toRecordsTable} 
            set label = null
            where place in (select abbreviation from config.place_special_incidents_index) and records_date > '{date}';''')
    print("label update completed!")
    connection.commit()

def weightDifferenceFromLastRaceUpdate(date, fromRecordsTable, toRecordsTable):
    cur.execute(rf'''DROP FUNCTION IF EXISTS weight_difference_from_last_race(text,date,double precision);
    CREATE OR REPLACE FUNCTION weight_difference_from_last_race(this_horse_id text, this_records_date date , this_declare_weight double precision)
RETURNS float AS $$
declare
  weight_difference float;
BEGIN
   select (this_declare_weight - declare_weight) as weight_difference from {fromRecordsTable}
 where {fromRecordsTable}.records_date < this_records_date and declare_weight_is_null is not true and horse_id = this_horse_id
 order by {fromRecordsTable}.records_date desc limit 1
   into weight_difference;
   RETURN weight_difference;
END;
$$ LANGUAGE plpgsql;''')
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS weight_difference_from_last_race float;''')
    cur.execute(rf'''update {toRecordsTable} 
    set weight_difference_from_last_race = alias.weight_difference_from_last_race
    from (select  "weight_difference_from_last_race"(horse_id, records_date, declare_weight) as weight_difference_from_last_race, records_id from {toRecordsTable} 
    where {toRecordsTable}.records_date > '{date}') as alias
    where {toRecordsTable}.records_id = alias.records_id;''')
    print("weight_difference_from_last_race update completed!")
    connection.commit()


def declarePower10Function():
    cur.execute(rf'''DROP FUNCTION IF EXISTS power10(float,float);
        CREATE OR REPLACE FUNCTION power10(no1 float, no2 float)
        RETURNS float AS $$
        declare
            result1 float;
        BEGIN
        
        select case 
        when (no1 - no2)/400 < -309 then 0
        when (no1 - no2)/400 > 308 then power(10,308)
        else power(10,(no1 - no2)/400) end as result1
        
        into result1;
           RETURN result1;
        END;
        $$ LANGUAGE plpgsql;''')


def horseOrJockeyOrTrainerEloUpdate(fromRecordsTable, toRecordsTable, kConstant, columnName, fieldName, allRace):
    print(f"calculating {fieldName} ...")
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName}_before float;''')
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName}_after float;''')
    declarePower10Function()
    eloFunctionUtil.declareFindHorseOrJockeyOrTrainerEloFromLastRaceFunction(fromRecordsTable, columnName, fieldName)
    eloFunctionUtil.declareHorseOrJockeyOrTrainerEloScoreFunction(toRecordsTable, columnName, fieldName)
    eloFunctionUtil.declareHorseOrJockeyOrTrainerEloEstimateScoreFunction(toRecordsTable, columnName, fieldName)
    eloFunctionUtil.declareHorseOrJockeyOrTrainerEloFunction(toRecordsTable, columnName, fieldName)
    connection.commit()
    count = 0
    for index, raceId in enumerate(allRace):
        count = count + 1
        cur.execute(rf'''select "compute_{fieldName}_with_k"({kConstant},alias.race_id)
             from (select distinct race_id, records_date from {toRecordsTable} 
             where race_id = '{raceId[0]}' and draw_is_null is not true order by records_date asc) as alias;''')
        print(f"compute_{fieldName}_with_k on {raceId[0]} update completed! Left: {len(allRace) - count}")
        connection.commit()


def horseOrJockeyOrTrainerWinRate(fromRecordsTable, toRecordsTable, columnName, fieldName, allRace):
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName} float;''')
    func = rf'''DROP FUNCTION IF EXISTS {fieldName}(text,text);
    CREATE OR REPLACE FUNCTION {fieldName}(this_{columnName} text, this_records_id text)
    RETURNS float AS $$
    declare
        {fieldName} float;
    BEGIN
    select
    case 
        when 
        total.total = 0 then 0 
        else
    (win.win/total.total) 
    end AS {fieldName}

    from(	
    select count(race_id)::float as total from {fromRecordsTable}
     where {columnName} = this_{columnName} 
     and records_id < this_records_id ) as total,

    (select count(race_id)::float as win from {fromRecordsTable}
     where {columnName} = this_{columnName} 
     and records_id < this_records_id
     and (place = '1' or place = '1 DH') ) as win

    into {fieldName};
       RETURN {fieldName};
    END;
    $$ LANGUAGE plpgsql;'''
    cur.execute(func)
    connection.commit()
    count = 0
    for index, raceId in enumerate(allRace):
        count = count + 1
        updateQuery = rf'''update {toRecordsTable} 
        set {fieldName} = alias.{fieldName}
        from (select "{fieldName}"({columnName}, records_id), records_id from {toRecordsTable} 
        where race_id = '{raceId[0]}' and draw_is_null is not true) as alias
        where {toRecordsTable}.records_id = alias.records_id;'''
        cur.execute(updateQuery)
        connection.commit()
        print(f"{fieldName} on {raceId[0]} update completed! Left: {len(allRace) - count}")

def horseOrJockeyOrTrainerPlaceRate(fromRecordsTable, toRecordsTable, columnName, fieldName, allRace):
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName} float;''')
    func = rf'''DROP FUNCTION IF EXISTS {fieldName}(text,text);
    CREATE OR REPLACE FUNCTION {fieldName}(this_{columnName} text, this_records_id text)
    RETURNS float AS $$
    declare
        {fieldName} float;
    BEGIN
    select
    case 
        when 
        total.total = 0 then 0 
        else
    (place.place/total.total) 
    end AS {fieldName}

    from(	
    select count(race_id)::float as total from {fromRecordsTable}
     where {columnName} = this_{columnName} 
     and records_id < this_records_id ) as total,

    (select count(race_id)::float as place from {fromRecordsTable}
     where {columnName} = this_{columnName} 
     and records_id < this_records_id
     and (place = '1' or place = '1 DH' or place = '2' or place = '2 DH' or place = '3' or place = '3 DH') ) as place

    into {fieldName};
       RETURN {fieldName};
    END;
    $$ LANGUAGE plpgsql;'''
    cur.execute(func)
    connection.commit()
    count = 0
    for index, raceId in enumerate(allRace):
        count = count + 1
        updateQuery = rf'''update {toRecordsTable} 
        set {fieldName} = alias.{fieldName}
        from (select "{fieldName}"({columnName}, records_id), records_id from {toRecordsTable} 
        where race_id = '{raceId[0]}' and draw_is_null is not true) as alias
        where {toRecordsTable}.records_id = alias.records_id;'''
        cur.execute(updateQuery)
        connection.commit()
        print(f"{fieldName} on {raceId[0]} update completed! Left: {len(allRace) - count}")


def updateJockeyTrainerWinRateForPrediction(fromRecordsTable, toRecordsTable, columnName, fieldName, allRace):
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName} float;''')
    func = rf'''DROP FUNCTION IF EXISTS {fieldName}(text,date);
        CREATE OR REPLACE FUNCTION {fieldName}(this_{columnName} text, this_records_date date)
        RETURNS float AS $$
        declare
            {fieldName} float;
        BEGIN
        select
        case 
            when 
            total.total = 0 then 0 
            else
        (win.win/total.total) 
        end AS {fieldName}

        from(	
        select count(race_id)::float as total from {fromRecordsTable}
         where {columnName} = this_{columnName} 
         and records_date < this_records_date ) as total,

        (select count(race_id)::float as win from {fromRecordsTable}
         where {columnName} = this_{columnName} 
         and records_date < this_records_date
         and (place = '1' or place = '1 DH') ) as win

        into {fieldName};
           RETURN {fieldName};
        END;
        $$ LANGUAGE plpgsql;'''
    cur.execute(func)
    connection.commit()
    count = 0
    for index, raceId in enumerate(allRace):
        count = count + 1
        updateQuery = rf'''update {toRecordsTable} 
            set {fieldName} = alias.{fieldName}
            from (select "{fieldName}"({columnName}, records_date), records_id from {toRecordsTable} 
            where race_id = '{raceId[0]}' and draw_is_null is not true) as alias
            where {toRecordsTable}.records_id = alias.records_id;'''
        cur.execute(updateQuery)
        connection.commit()
        print(f"{fieldName} on {raceId[0]} update completed! Left: {len(allRace) - count}")

def updateJockeyTrainerPlaceRateForPrediction(fromRecordsTable, toRecordsTable, columnName, fieldName, allRace):
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName} float;''')
    func = rf'''DROP FUNCTION IF EXISTS {fieldName}(text,date);
        CREATE OR REPLACE FUNCTION {fieldName}(this_{columnName} text, this_records_date date)
        RETURNS float AS $$
        declare
            {fieldName} float;
        BEGIN
        select
        case 
            when 
            total.total = 0 then 0 
            else
        (win.win/total.total) 
        end AS {fieldName}

        from(	
        select count(race_id)::float as total from {fromRecordsTable}
         where {columnName} = this_{columnName} 
         and records_date < this_records_date ) as total,

        (select count(race_id)::float as win from {fromRecordsTable}
         where {columnName} = this_{columnName} 
         and records_date < this_records_date
         and (place = '1' or place = '1 DH' or place = '2' or place = '2 DH' or place = '3' or place = '3 DH') ) as win

        into {fieldName};
           RETURN {fieldName};
        END;
        $$ LANGUAGE plpgsql;'''
    cur.execute(func)
    connection.commit()
    count = 0
    for index, raceId in enumerate(allRace):
        count = count + 1
        updateQuery = rf'''update {toRecordsTable} 
            set {fieldName} = alias.{fieldName}
            from (select "{fieldName}"({columnName}, records_date), records_id from {toRecordsTable} 
            where race_id = '{raceId[0]}' and draw_is_null is not true) as alias
            where {toRecordsTable}.records_id = alias.records_id;'''
        cur.execute(updateQuery)
        connection.commit()
        print(f"{fieldName} on {raceId[0]} update completed! Left: {len(allRace) - count}")

def updateJockeyTrainerEloForPrediction(fromRecordsTable, toRecordsTable, columnName, fieldName, allRace):
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName}_before_for_prediction float;''')
    eloFunctionUtil.declareFindHorseOrJockeyOrTrainerEloFromLastRaceByDateFunction(fromRecordsTable, columnName, fieldName)
    cur.execute(rf'''CREATE OR REPLACE FUNCTION update_{fieldName}_before_for_prediction(this_race_id text)
            RETURNS void AS $$
            BEGIN 
            update {toRecordsTable}  
            set {fieldName}_before_for_prediction = alias.{fieldName}_last_race_by_date
            from (select "{fieldName}_last_race_by_date"({columnName}, records_date), records_id from {toRecordsTable} where race_id = this_race_id) as alias
            where alias.records_id = {toRecordsTable}.records_id;
            END;
            $$ LANGUAGE plpgsql;''')
    connection.commit()
    count = 0
    for index, raceId in enumerate(allRace):
        count = count + 1
        cur.execute(rf'''select "update_{fieldName}_before_for_prediction"(alias.race_id)
                 from (select distinct race_id from {toRecordsTable} 
                 where race_id = '{raceId[0]}' and draw_is_null is not true ) as alias;''')
        print(f"update_{fieldName}_before_for_prediction on {raceId[0]} update completed! Left: {len(allRace) - count}")
        connection.commit()


def horseRaceHaveTaken(fromRecordsTable, toRecordsTable, allRace):
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS horse_race_have_taken float;''')
    cur.execute(rf'''CREATE OR REPLACE FUNCTION count_horse_race_have_taken(this_records_date date, this_horse_id text)
                RETURNS float AS $$
            declare
                horse_race_have_taken float;
            BEGIN
            select count(race_id) as horse_race_have_taken from {fromRecordsTable} where records_date < this_records_date and horse_id = this_horse_id
            into horse_race_have_taken;
               RETURN horse_race_have_taken;
            END;
            $$ LANGUAGE plpgsql;''')
    connection.commit()
    count = 0
    for index, raceId in enumerate(allRace):
        count = count + 1
        cur.execute(rf'''
                 update {toRecordsTable} set horse_race_have_taken = alias.this_horse_race_have_taken
                 from (select "count_horse_race_have_taken"(records_date, horse_id) as this_horse_race_have_taken, race_id, records_id
                 from {toRecordsTable} where race_id = '{raceId[0]}') as alias
                 where {toRecordsTable}.records_id = alias.records_id;''')
        print(f"count_horse_race_have_taken on {raceId[0]} update completed! Left: {len(allRace) - count}")
        connection.commit()

def bestFinishTimeOnSameDistance(fromRecordsTable, toRecordsTable, fromRaceTable, toRaceTable, allRace):
    fieldName = 'best_finish_time_on_same_distance'
    funcQuery = rf'''DROP FUNCTION IF EXISTS {fieldName}(text,date,text);
CREATE OR REPLACE FUNCTION {fieldName}(this_horse_id text, this_records_date date , this_race_distance text)
RETURNS time AS $$
declare
	{fieldName} time;
BEGIN
    select finish_time from {fromRecordsTable}, {fromRaceTable} 
    where {fromRecordsTable}.race_id = {fromRaceTable}.race_id 
    and {fromRecordsTable}.records_date < this_records_date 
    and horse_id = this_horse_id 
    and distance = this_race_distance order by finish_time asc limit 1
   into {fieldName};
   RETURN {fieldName};
END;
$$ LANGUAGE plpgsql;
'''
    cur.execute(funcQuery)
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName} time;''')
    count = 0
    for index, raceId in enumerate(allRace):
        count = count + 1
        updateQuery = rf'''update {toRecordsTable} 
            set {fieldName} = alias.{fieldName}
            from (select "{fieldName}"(horse_id,records_date,distance) as {fieldName}, {toRecordsTable}.records_id from {toRaceTable}, {toRecordsTable} 
            where {toRaceTable}.race_id = {toRecordsTable}.race_id and {toRecordsTable}.race_id = '{raceId[0]}') as alias
            where {toRecordsTable}.records_id = alias.records_id;'''
        cur.execute(updateQuery)
        print(f"{fieldName} update of {raceId[0]} completed! Left {len(allRace) - count}")
        connection.commit()

def pastPlace(fromRecordsTable, toRecordsTable, allRaceId, whichPastRace):
    fieldName = f'past_place_{whichPastRace}'
    funcQuery = rf'''DROP FUNCTION IF EXISTS {fieldName}(text,date);
    CREATE OR REPLACE FUNCTION {fieldName}(this_horse_id text, this_records_date date)
    RETURNS text AS $$
    declare
    	{fieldName} text;
    BEGIN
       select place from {fromRecordsTable} 
     where horse_id = this_horse_id
     and records_date < this_records_date
     order by records_date desc limit 1 offset {whichPastRace - 1}
       into {fieldName};
       RETURN {fieldName};
    END;
    $$ LANGUAGE plpgsql;
    '''
    cur.execute(funcQuery)
    cur.execute(rf'''ALTER TABLE {toRecordsTable} ADD COLUMN IF NOT EXISTS {fieldName} text;''')
    count = 0
    for index, raceId in enumerate(allRaceId):
        count = count + 1
        updateQuery = rf'''update {toRecordsTable} 
                set {fieldName} = alias.{fieldName}
                from (select "{fieldName}"(horse_id,records_date) as {fieldName}, records_id from {toRecordsTable} 
                where race_id = '{raceId[0]}') as alias
                where {toRecordsTable}.records_id = alias.records_id;'''
        cur.execute(updateQuery)
        print(f"{fieldName} update of {raceId[0]} completed! Left {len(allRaceId) - count}")
        connection.commit()