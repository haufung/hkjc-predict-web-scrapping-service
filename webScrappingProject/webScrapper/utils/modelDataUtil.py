import pandas as pd

from django.db import connection

from . import modelRequiredHeaderUtil

cur = connection.cursor()

def loadTrainingSetOrderByRecordsDateBeforeYear(recordsTable, raceTable, year, version):
    records_header_required = modelRequiredHeaderUtil.recordsHeader(version)
    race_header_required = modelRequiredHeaderUtil.raceHeader(version)
    sqlQuery = rf'''select {",".join(records_header_required)}, {",".join(race_header_required)}
    from {recordsTable},{raceTable} 
    where {recordsTable}.race_id = {raceTable}.race_id 
    and records_date < (select records_date from {recordsTable} order by records_date desc limit 1) - interval '{year} year' and label is not null;'''
    return pd.read_sql(sqlQuery, connection)


def loadTestingSetOrderByRecordsDate(recordsTable, raceTable, version):
    records_header_required = modelRequiredHeaderUtil.recordsHeader(version)
    race_header_required = modelRequiredHeaderUtil.raceHeader(version)
    sqlQuery = rf'''select {",".join(records_header_required)}, {",".join(race_header_required)}
    from {recordsTable},{raceTable} 
    where {recordsTable}.race_id = {raceTable}.race_id 
    and records_date > (select records_date from {recordsTable} order by records_date desc limit 1) - interval '1 year' and label is not null
    order by {recordsTable}.race_id desc;'''
    return pd.read_sql(sqlQuery, connection)

def loadTestingSetOrderByRecordsDateForPrediction(recordsTable, raceTable, version):
    records_header_required_for_prediction = modelRequiredHeaderUtil.recordsHeaderForPrediction(version)
    race_header_required = modelRequiredHeaderUtil.raceHeader(version)
    sqlQuery = rf'''select {",".join(records_header_required_for_prediction)}, {",".join(race_header_required)}
    from {recordsTable},{raceTable} 
    where {recordsTable}.race_id = {raceTable}.race_id 
    and records_date > (select records_date from {recordsTable} order by records_date desc limit 1) - interval '1 year' and label is not null
    order by {recordsTable}.race_id desc;'''
    return pd.read_sql(sqlQuery, connection)

def loadNewDataWithinAYearForPrediction(recordsTable, raceTable, version):
    records_header_required_for_prediction = modelRequiredHeaderUtil.recordsHeaderForPrediction(version)
    race_header_required = modelRequiredHeaderUtil.raceHeader(version)
    versionName = version.replace(".", "_")
    sqlQuery = rf'''select {",".join(records_header_required_for_prediction)}, {",".join(race_header_required)}
    from {recordsTable},{raceTable} 
    where {recordsTable}.race_id = {raceTable}.race_id 
    and records_date > (select records_date from {recordsTable} order by records_date desc limit 1) - interval '1 year' and label is not null and prediction_{versionName} is null
    order by {recordsTable}.race_id desc;'''
    return pd.read_sql(sqlQuery, connection)

# select model.records.records_id, model.records.prediction - predict.current_records.prediction as diff from model.records, predict.current_records
# where model.records.prediction != predict.current_records.prediction
# and  model.records.records_id = predict.current_records.records_id
# and predict.current_records.records_date = '2020-10-11'
# order by diff desc

# select prediction, records_id,actual_weight,normalized_declare_weight,draw,normalized_horse_elo_before,normalized_jockey_elo_before_for_prediction,normalized_trainer_elo_before_for_prediction,horse_win_rate,jockey_win_rate_for_prediction,trainer_win_rate_for_prediction,days_apart_from_previous_race,first_last_place_on_same_course,second_last_place_on_same_course,third_last_place_on_same_course,normalized_weight_difference_from_last_race,horse_days_from_first_race,label, location,distance,going,course
#     from model.records,model.race
#     where model.records.race_id = model.race.race_id
#     and records_date > (select records_date from model.records order by records_date desc limit 1) - interval '1 year'
# 	and model.records.records_id = '20201011_09_B464'
#     order by model.records.race_id desc;

# select prediction, records_id,actual_weight,normalized_declare_weight,draw,normalized_horse_elo_before,normalized_jockey_elo_before_for_prediction,normalized_trainer_elo_before_for_prediction,horse_win_rate,jockey_win_rate_for_prediction,trainer_win_rate_for_prediction,days_apart_from_previous_race,first_last_place_on_same_course,second_last_place_on_same_course,third_last_place_on_same_course,normalized_weight_difference_from_last_race,horse_days_from_first_race,label, location,distance,going,course
#     from predict.current_records,model.race
#     where predict.current_records.race_id = model.race.race_id
#     and records_date > (select records_date from predict.current_records order by records_date desc limit 1) - interval '1 year'
# 	and predict.current_records.records_id = '20201011_09_B464'
#     order by predict.current_records.race_id desc;

def loadValidationSetOrderByRecordsDateAfterYear(recordsTable, raceTable, year, version):
    records_header_required = modelRequiredHeaderUtil.recordsHeader(version)
    race_header_required = modelRequiredHeaderUtil.raceHeader(version)
    sqlQuery = rf'''select {",".join(records_header_required)}, {",".join(race_header_required)}
    from {recordsTable},{raceTable} 
    where {recordsTable}.race_id = {raceTable}.race_id 
    and records_date > (select records_date from {recordsTable} order by records_date desc limit 1) - interval '{year} year'
    and records_date < (select records_date from {recordsTable} order by records_date desc limit 1) - interval '1 year' and label is not null
    order by {recordsTable}.race_id desc;'''
    return pd.read_sql(sqlQuery, connection)


def loadSetByRaceId(recordsTable, raceTable, raceId, version):
    records_header_required = modelRequiredHeaderUtil.recordsHeader(version)
    race_header_required = modelRequiredHeaderUtil.raceHeader(version)
    sqlQuery = rf'''select {",".join(records_header_required)}, {",".join(race_header_required)}
    from {recordsTable},{raceTable} 
    where {recordsTable}.race_id = {raceTable}.race_id 
    and {recordsTable}.race_id = '{raceId}' and label is not null
    order by {recordsTable}.race_id desc;'''
    return pd.read_sql(sqlQuery, connection)
