from django.db import connection

from ..utils import calculatedFieldUtil, dataNormalizationUtil

cur = connection.cursor()


def calculationBeforePredict(updateAfterDateString, fromRaceTable, toRaceTable, fromRecordsTable, toRecordsTable,
                             initCalculate):
    calculatedFieldUtil.dayApartFromPreviousRaceUpdate(updateAfterDateString, fromRecordsTable, toRecordsTable,
                                                       initCalculate)
    calculatedFieldUtil.weightDifferenceFromLastRaceUpdate(updateAfterDateString, fromRecordsTable, toRecordsTable)
    calculatedFieldUtil.horseDaysFromFirstRaceUpdate(updateAfterDateString, fromRecordsTable, toRecordsTable)
    cur.execute(
        rf'''select distinct race_id, records_date from {toRecordsTable} where records_date > '{updateAfterDateString}' order by race_id asc;''')
    allRaceId = cur.fetchall()
    if len(allRaceId) > 0:
        calculatedFieldUtil.lastPlaceOnSameCourseUpdate(fromRecordsTable, toRecordsTable, fromRaceTable, toRaceTable,
                                                        'first', allRaceId)
        calculatedFieldUtil.lastPlaceOnSameCourseUpdate(fromRecordsTable, toRecordsTable, fromRaceTable, toRaceTable,
                                                        'second', allRaceId)
        calculatedFieldUtil.lastPlaceOnSameCourseUpdate(fromRecordsTable, toRecordsTable, fromRaceTable, toRaceTable,
                                                        'third', allRaceId)
        calculatedFieldUtil.updateNullCalculation(updateAfterDateString, toRecordsTable)
        calculatedFieldUtil.horseOrJockeyOrTrainerEloUpdate(fromRecordsTable, toRecordsTable, 2250, 'horse_id',
                                                            'horse_elo', allRaceId)
        calculatedFieldUtil.horseOrJockeyOrTrainerEloUpdate(fromRecordsTable, toRecordsTable, 50, 'jockey_name',
                                                            'jockey_elo', allRaceId)
        calculatedFieldUtil.horseOrJockeyOrTrainerEloUpdate(fromRecordsTable, toRecordsTable, 25, 'trainer_name',
                                                            'trainer_elo', allRaceId)

        dataNormalizationUtil.normalizeElo(updateAfterDateString, 'horse', toRecordsTable)
        dataNormalizationUtil.normalizeElo(updateAfterDateString, 'jockey', toRecordsTable)
        dataNormalizationUtil.normalizeElo(updateAfterDateString, 'trainer', toRecordsTable)
        dataNormalizationUtil.normalizeWeightDifferenceFromLastRace(updateAfterDateString, toRecordsTable)
        dataNormalizationUtil.normalizeDeclareWeight(updateAfterDateString, toRecordsTable)
        dataNormalizationUtil.normalizeActualWeight(updateAfterDateString, toRecordsTable)
        dataNormalizationUtil.normalizeDaysApartFromPreviousRace(updateAfterDateString, toRecordsTable)

        calculatedFieldUtil.horseOrJockeyOrTrainerWinRate(fromRecordsTable, toRecordsTable, 'horse_id',
                                                          'horse_win_rate', allRaceId)
        calculatedFieldUtil.horseOrJockeyOrTrainerWinRate(fromRecordsTable, toRecordsTable,
                                                          'jockey_name',
                                                          'jockey_win_rate', allRaceId)
        calculatedFieldUtil.horseOrJockeyOrTrainerWinRate(fromRecordsTable, toRecordsTable,
                                                          'trainer_name', 'trainer_win_rate', allRaceId)
        calculatedFieldUtil.horseOrJockeyOrTrainerPlaceRate(fromRecordsTable, toRecordsTable, 'horse_id',
                                                            'horse_place_rate', allRaceId)
        calculatedFieldUtil.horseOrJockeyOrTrainerPlaceRate(fromRecordsTable, toRecordsTable,
                                                            'jockey_name',
                                                            'jockey_place_rate', allRaceId)
        calculatedFieldUtil.horseOrJockeyOrTrainerPlaceRate(fromRecordsTable, toRecordsTable,
                                                            'trainer_name', 'trainer_place_rate', allRaceId)
        calculatedFieldUtil.horseRaceHaveTaken(fromRecordsTable, toRecordsTable, allRaceId)
        dataNormalizationUtil.normalizeHorseRaceHaveTaken(updateAfterDateString, toRecordsTable)

        calculatedFieldUtil.bestFinishTimeOnSameDistance(fromRecordsTable, toRecordsTable, fromRaceTable, toRaceTable, allRaceId)

        for i in range(6):
            whichPastRace = i + 1
            calculatedFieldUtil.pastPlace(fromRecordsTable, toRecordsTable, allRaceId, whichPastRace)


def calculationForPredict(updateAfterDateString, fromRecordsTable, toRecordsTable):
    cur.execute(
        rf'''select distinct race_id, records_date from {toRecordsTable} where records_date > '{updateAfterDateString}' order by race_id asc;''')
    allRaceId = cur.fetchall()
    calculatedFieldUtil.updateJockeyTrainerEloForPrediction(fromRecordsTable, toRecordsTable, 'jockey_name',
                                                            'jockey_elo', allRaceId)
    calculatedFieldUtil.updateJockeyTrainerEloForPrediction(fromRecordsTable, toRecordsTable, 'trainer_name',
                                                            'trainer_elo', allRaceId)
    calculatedFieldUtil.updateJockeyTrainerWinRateForPrediction(fromRecordsTable, toRecordsTable, 'jockey_name',
                                                                'jockey_win_rate_for_prediction',
                                                                allRaceId)
    calculatedFieldUtil.updateJockeyTrainerWinRateForPrediction(fromRecordsTable, toRecordsTable, 'trainer_name',
                                                                'trainer_win_rate_for_prediction',
                                                                allRaceId)
    dataNormalizationUtil.normalizeEloForPrediction(updateAfterDateString, 'jockey', toRecordsTable)
    dataNormalizationUtil.normalizeEloForPrediction(updateAfterDateString, 'trainer', toRecordsTable)
    calculatedFieldUtil.updateJockeyTrainerPlaceRateForPrediction(fromRecordsTable, toRecordsTable, 'jockey_name',
                                                                  'jockey_place_rate_for_prediction',
                                                                  allRaceId)
    calculatedFieldUtil.updateJockeyTrainerPlaceRateForPrediction(fromRecordsTable, toRecordsTable, 'trainer_name',
                                                                  'trainer_place_rate_for_prediction',
                                                                  allRaceId)
