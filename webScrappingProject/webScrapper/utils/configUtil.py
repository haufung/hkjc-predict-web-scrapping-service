from django.db import connection

cur = connection.cursor()


def allVersion():
    cur.execute(rf''' select version from config.model_version;''')
    versionList = [r[0] for r in cur.fetchall()]
    return versionList

def allReadyVersion():
    cur.execute(rf''' select version from config.model_version where is_ready = true;''')
    versionList = [r[0] for r in cur.fetchall()]
    return versionList

def earliestModelStartDate():
    cur.execute(rf'''select date from config.model_start_date order by date asc limit 1;''')
    versionList = [r[0] for r in cur.fetchall()]
    return versionList

def modelThreshold(version):
    cur.execute(rf'''select threshold from config.model_threshold where version = %s;''', [version])
    thresholdList = [r[0] for r in cur.fetchall()]
    return thresholdList

def modelStartDate(version):
    cur.execute(rf'''select date from config.model_start_date where version = %s;''', [version])
    dateList = [r[0] for r in cur.fetchall()]
    return dateList

def updateModelStartDate(version):
    cur.execute(rf'''select records_date from model.records where records_date > (select records_date from model.records order by records_date desc limit 1) - interval '1 year' and label is not null order by records_date asc limit 1;''')
    dateList = [r[0] for r in cur.fetchall()]
    cur.execute(rf'''update config.model_start_date set date = %s where version = %s;''', [dateList[0], version])
    connection.commit()

def latestUpdateDate():
    cur.execute(rf'''select date from config.latest_updated_date where id = 1;''')
    return cur.fetchall()[0][0]

def updateLatestUpdateDate():
    cur.execute(rf'''update config.latest_updated_date set date = (select records_date from model.records order by race_id desc limit 1) where id = 1;''')
    connection.commit()
