import environ
from django.db import connection

env = environ.Env()

cur = connection.cursor()


def removeFromDebugLogByUrl(url):
    cur.execute(rf'''delete from init.debug_log where url = %s;''', [url])
    connection.commit()


def updateRaceDebugLog(scrapUrl, errorMessage, reference, generalReference, operation):
    cur.execute(
        f'''INSERT INTO init.race_debug_log (url, error_message, created_date, reference, general_reference, operation) VALUES (%s,%s, NOW() AT TIME ZONE '{env("TIMEZONE")}', %s, %s, %s);''',
        [scrapUrl, errorMessage, reference, generalReference, operation])
    connection.commit()


def updateHorseDebugLog(errorMessage, reference):
    cur.execute(
        f'''INSERT INTO init.horse_debug_log (error_message, created_date, reference) VALUES (%s, NOW() AT TIME ZONE '{env("TIMEZONE")}', %s);''',
        [errorMessage, reference])
    connection.commit()


def insertServiceLog(path):
    cur.execute(
        f'''INSERT INTO service.job_status_log (path, start_date, status) VALUES (%s, NOW() AT TIME ZONE '{env("TIMEZONE")}', %s) RETURNING id;''',
        [path, "PROGRESSING"])
    connection.commit()
    return cur.fetchone()[0]


def updateServiceLog(logId, status, message, errorStack):
    cur.execute(
        f'''UPDATE service.job_status_log SET status = %s, complete_date = NOW() AT TIME ZONE '{env("TIMEZONE")}', message = %s, error_stack = %s where id = %s;''',
        [status, message, errorStack, logId])
    connection.commit()


def insertRaceScrappingLog(raceId):
    cur.execute(
        f'''INSERT INTO service.race_scrapping_log (race_id, start_date, status) VALUES (%s, NOW() AT TIME ZONE '{env("TIMEZONE")}', %s) RETURNING id;''',
        [raceId, "PROGRESSING"])
    connection.commit()
    return cur.fetchone()[0]


def updateRaceScrappingLog(logId, status, message, errorStack):
    cur.execute(
        f'''UPDATE service.race_scrapping_log SET status = %s, complete_date = NOW() AT TIME ZONE '{env("TIMEZONE")}', message = %s, error_stack = %s where id = %s;''',
        [status, message, errorStack, logId])
    connection.commit()

def insertHorseScrappingLog(url, horseName):
    cur.execute(
        f'''INSERT INTO service.horse_scrapping_log (url, horse_name, start_date, status) VALUES (%s, %s, NOW() AT TIME ZONE '{env("TIMEZONE")}', %s) RETURNING id;''',
        [url, horseName, "PROGRESSING"])
    connection.commit()
    return cur.fetchone()[0]


def updateHorseScrappingLog(logId, horseId, status, message, errorStack):
    cur.execute(
        f'''UPDATE service.horse_scrapping_log SET status = %s, complete_date = NOW() AT TIME ZONE '{env("TIMEZONE")}', message = %s, error_stack = %s, horse_id = %s where id = %s;''',
        [status, message, errorStack, horseId, logId])
    connection.commit()

