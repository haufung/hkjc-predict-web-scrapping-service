from django.db import connection
from ..utils import picklistValueUtil

cur = connection.cursor()

def queryDistinctRaceCourse(raceTable):
    cur.execute(rf'''select distinct(course) from {raceTable};''')
    distinctValue = [r[0] for r in cur.fetchall()]
    return distinctValue


def queryDistinctRaceDistance(raceTable):
    cur.execute(rf'''select distinct(distance) from {raceTable};''')
    distinctValue = [r[0] for r in cur.fetchall()]
    return distinctValue


def queryDistinctRaceGoing(raceTable):
    cur.execute(rf'''select distinct(going) from {raceTable};''')
    distinctValue = [r[0] for r in cur.fetchall()]
    return distinctValue


def queryDistinctRaceLocation(raceTable):
    cur.execute(rf'''select distinct(location) from {raceTable};''')
    distinctValue = [r[0] for r in cur.fetchall()]
    return distinctValue


def queryDistinctRecordsDraw(recordsTable):
    cur.execute(rf'''select distinct(draw) from {recordsTable};''')
    distinctValue = [r[0] for r in cur.fetchall()]
    return distinctValue

def queryDistinctRecordsLastPlaceOnSameCourse(recordsTable):
    cur.execute(rf'''select distinct(place) from {recordsTable};''')
    distinctValue = [r[0] for r in cur.fetchall()]
    return distinctValue


def validatePickListValue(pickList, toValidateList):
    diffList = list(set(toValidateList) - set(pickList))
    validationSuccess = True
    for value in diffList:
        print(f'"{value}" should not exist among {",".join(pickList)}')
        validationSuccess = False
    return validationSuccess


def validatePickListValueForTable(raceTable, recordsTable):
    raceLocationPicklist = picklistValueUtil.raceLocationPicklist()
    raceDistancePicklist = picklistValueUtil.raceDistancePicklist()
    raceGoingPicklist = picklistValueUtil.raceGoingPicklist()
    raceCoursePicklist = picklistValueUtil.raceCoursePicklist()
    recordsDrawPicklist = picklistValueUtil.recordsDrawPicklist()
    recordsLastPlaceOnSameCoursePicklist = picklistValueUtil.recordsLastPlaceOnSameCoursePicklist()

    distinctRaceLocation = queryDistinctRaceLocation(raceTable)
    distinctRaceDistance = queryDistinctRaceDistance(raceTable)
    distinctRaceGoing = queryDistinctRaceGoing(raceTable)
    distinctRaceCourse = queryDistinctRaceCourse(raceTable)
    distinctRecordsDraw = queryDistinctRecordsDraw(recordsTable)
    distinctRecordsLastPlaceOnSameCourse = queryDistinctRecordsLastPlaceOnSameCourse(recordsTable)

    validationSuccess = [validatePickListValue(raceLocationPicklist, distinctRaceLocation),
                         validatePickListValue(raceDistancePicklist, distinctRaceDistance),
                         validatePickListValue(raceGoingPicklist, distinctRaceGoing),
                         validatePickListValue(raceCoursePicklist, distinctRaceCourse),
                         validatePickListValue(recordsDrawPicklist, distinctRecordsDraw),
                         validatePickListValue(recordsLastPlaceOnSameCoursePicklist,
                                               distinctRecordsLastPlaceOnSameCourse)]
    return False not in validationSuccess
