from django.db import connection

cur = connection.cursor()

DB_TO_STORE_PICKLIST = 'config.'


def raceCoursePicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}race_course_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist


def raceDistancePicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}race_distance_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist


def raceGoingPicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}race_going_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist


def raceLocationPicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}race_location_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist


def recordsDrawPicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}records_draw_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist

def recordsLastPlaceOnSameCoursePicklist():
    cur.execute(rf''' select * from {DB_TO_STORE_PICKLIST}records_last_place_on_same_course_picklist''')
    picklist = [r[0] for r in cur.fetchall()]
    return picklist

