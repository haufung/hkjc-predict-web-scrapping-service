import re
import traceback

from bs4 import BeautifulSoup
from django.db import connection

from ..utils import debugLogUtil, seleniumUtil

cur = connection.cursor()

# Deprecated
column_one_css_selector = 'body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(2) > td.table_eng_text > ul > li > a'
column_two_key_css_selector = 'body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(2) > table > tbody > tr > td:nth-child(1)'
column_two_value_css_selector = 'body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(2) > table > tbody > tr > td:nth-child(3)'
column_three_key_css_selector = 'body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(3) > table > tbody > tr > td:nth-child(1)'
column_three_value_css_selector = 'body > div > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(3) > table > tbody > tr > td:nth-child(3)'
horse_id_and_name_selector = "body > div > div > table.horseProfile > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(1) > td > span"
brand_number_form_selector = 'table.bigborder input[name="BrandNumber"]'

column_one_css_selector_1 = '#innerContent > div.commContent > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(2) > td.table_eng_text > ul > li > a'
column_two_key_css_selector_1 = '#innerContent > div.commContent > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(2) > table > tbody > tr > td:nth-child(1)'
column_two_value_css_selector_1 = '#innerContent > div.commContent > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(2) > table > tbody > tr > td:nth-child(3)'
column_three_key_css_selector_1 = '#innerContent > div.commContent > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(3) > table > tbody > tr > td:nth-child(1)'
column_three_value_css_selector_1 = '#innerContent > div.commContent > div:nth-child(1) > table.horseProfile > tbody > tr > td:nth-child(3) > table > tbody > tr > td:nth-child(3)'
horse_id_and_name_selector_1 = "#innerContent > div.commContent > div > table.horseProfile > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(1) > td > span"
chinese_language_selector_1 = "#topNav > div:nth-child(1) > a.lang"
brand_number_form_selector_1 = 'input[name="BrandNumber"]'
brand_number_search_selector_1 = 'input#submit1.table_eng_text'

def insertAllHorseUrlTable(horseId, url):
    cur.execute(rf'''INSERT INTO init.all_horse_url
            ("horse_id",
            "url")
            VALUES
            (%s, %s)''', [horseId, url])
    connection.commit()


def queryDistinctHorseIdFromRecords():
    cur.execute(rf'''select distinct horse_id from init.init_records where horse_id not in (select horse_id from init.all_horse_url)''')
    return cur.fetchall()


def generateHorseLink(dri, horseIdList):
    search_horse_url = "http://racing.hkjc.com/racing/information/english/Horse/SelectHorse.aspx"
    for horseId in horseIdList:
        dri.get(search_horse_url)
        seleniumUtil.checkLoading(dri, brand_number_form_selector_1)
        form = dri.find_element_by_css_selector(brand_number_form_selector_1)
        form.send_keys(horseId)
        button = dri.find_element_by_css_selector(brand_number_search_selector_1)
        button.click()
        seleniumUtil.checkLoading(dri)
        soup = BeautifulSoup(dri.page_source, 'html.parser')
        if f'No such record "{horseId}" found' in soup.get_text():
            debugLogUtil.updateHorseDebugLog(horseId, "NO RECORD FOUND WHEN GETTING URL")
        else:
            insertAllHorseUrlTable(horseId, dri.current_url)


def insertIntoInitHorseTable(horseTable, horse_id, english_name, trackwork_records_url, veterinary_records_url, age,
                             country_of_origin, colour, sex, import_type, sire, dam, dam_sire, chinese_name):
    cur.execute(rf'''INSERT INTO {horseTable}
            ("horse_id",
            "english_name",
            "trackwork_records_url",
            "veterinary_records_url",
            "age",
            "country_of_origin",
            "colour",
            "sex",
            "import_type",
            "sire",
            "dam",
            "dam_sire",
            "chinese_name")
            VALUES
        (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)''',
                [horse_id, english_name, trackwork_records_url, veterinary_records_url, age,
                 country_of_origin, colour, sex, import_type, sire, dam, dam_sire, chinese_name])
    connection.commit()


def validateWebsiteAndDBHorseId(url, hid1, hidFromDB):
    if hid1 != hidFromDB:
        debugLogUtil.updateHorseDebugLog(f"Validation Failed: {hid1} and {hidFromDB}", url)
        return False
    else:
        return True

def scrapHorseData(dri, url, horseTable, horseIdFromDB, horseName):
    logId = debugLogUtil.insertHorseScrappingLog(dri.current_url, horseName)
    horseId = None
    try:
        dri.get(url)
        seleniumUtil.checkLoading(dri)
        soup = BeautifulSoup(dri.page_source, 'html.parser')
        if seleniumUtil.checkSoupHaveCookieChecking(soup):
            if seleniumUtil.refreshForCookieChecking(dri.current_url, dri):
                if horseIdFromDB is not None:
                    debugLogUtil.updateHorseDebugLog(horseIdFromDB, "HORSE HAVE COOKIE CHECKING")
                else:
                    debugLogUtil.updateHorseDebugLog(horseName, "HORSE HAVE COOKIE CHECKING")
                return
            else:
                soup = BeautifulSoup(dri.page_source, 'html.parser')

        if seleniumUtil.checkSoupHaveNoInformation(soup):
            if seleniumUtil.refreshForNoInformation(dri.current_url, dri):
                if horseIdFromDB is not None:
                    debugLogUtil.updateHorseDebugLog(horseIdFromDB, "HORSE HAVE COOKIE CHECKING")
                else:
                    debugLogUtil.updateHorseDebugLog(horseName, "HORSE HAVE COOKIE CHECKING")
                return
            else:
                soup = BeautifulSoup(dri.page_source, 'html.parser')

        trackworkRecordsUrl = None
        veterinaryRecordsUrl = None
        age = None
        countryOfOrigin = None
        colour = None
        sex = None
        importType = None
        sire = None
        dam = None
        damSire = None
        horseId = soup.select(horse_id_and_name_selector_1)[0].text
        horseId = re.findall(r"\(([A-Za-z0-9_]+)\)", horseId)[0]
        if horseIdFromDB is None:
            print(f"Inserting into all_horse_url ... {horseId}")
            insertAllHorseUrlTable(horseId, url)
        if horseIdFromDB is not None and validateWebsiteAndDBHorseId(dri.current_url, horseId, horseIdFromDB) is False:
            return

        englishName = soup.select(horse_id_and_name_selector_1)[0].text
        englishName = englishName.split(" (")[0]

        column1 = soup.select(column_one_css_selector)
        column2Key = soup.select(column_two_key_css_selector)
        column2Value = soup.select(column_two_value_css_selector)
        column3Key = soup.select(column_three_key_css_selector)
        column3Value = soup.select(column_three_value_css_selector)
        for row1 in column1:
            if row1.get_text() == 'Trackwork Records':
                trackworkRecordsUrl = "http://racing.hkjc.com" + row1['href']
            elif row1.get_text() == 'Veterinary Records':
                veterinaryRecordsUrl = "http://racing.hkjc.com" + row1['href']

        for index2, row2 in enumerate(column2Key):
            if row2.get_text() == 'Country of Origin':
                countryOfOrigin = column2Value[index2].text
            elif row2.get_text() == 'Country of Origin / Age':
                originAndAge = column2Value[index2].text
                originAndAge = originAndAge.split(" / ")
                if len(originAndAge) == 2:
                    age = originAndAge.pop()
                    countryOfOrigin = originAndAge.pop()
                elif len(originAndAge) < 2:
                    debugLogUtil.updateHorseDebugLog("MISSING AGE OR ORIGIN", horseId)
                    countryOfOrigin = originAndAge.pop()
                elif len(originAndAge) > 2:
                    debugLogUtil.updateHorseDebugLog("SOMETHING WRONG WITH AGE OR ORIGIN", horseId)
            elif row2.get_text() == 'Colour / Sex':
                colourAndSex = column2Value[index2].text
                colourAndSex = colourAndSex.split(" / ")
                if len(colourAndSex) > 2:
                    sex = colourAndSex.pop()
                    colour = "/".join(colourAndSex)
                elif len(colourAndSex) == 2:
                    colour = colourAndSex[0]
                    sex = colourAndSex[1]
                else:
                    debugLogUtil.updateHorseDebugLog("SOMETHING WRONG WITH COLOUR AND SEX", horseId)
            elif row2.get_text() == 'Import Type':
                importType = column2Value[index2].text
                importType = "".join(re.findall("[a-zA-Z]", importType))

            for index3, row3 in enumerate(column3Key):
                if row3.get_text() == 'Sire':
                    sire = column3Value[index3].text
                    sire = " ".join(re.findall(r"\w+|'|-", sire))
                elif row3.get_text() == 'Dam':
                    dam = column3Value[index3].text
                    dam = " ".join(re.findall(r"\w+|'|-", dam))
                elif row3.get_text() == '''Dam's Sire''':
                    damSire = column3Value[index3].text
                    damSire = " ".join(re.findall(r"\w+|'|-", damSire))

        button = dri.find_element_by_css_selector(chinese_language_selector_1)
        button.click()
        seleniumUtil.checkLoading(dri)
        soup = BeautifulSoup(dri.page_source, 'html.parser')
        if seleniumUtil.checkSoupHaveCookieChecking(soup):
            if seleniumUtil.refreshForCookieChecking(dri.current_url, dri):
                debugLogUtil.updateHorseDebugLog(horseId, "HORSE CHINESE NAME HAVE COOKIE CHECKING")
                return
            else:
                soup = BeautifulSoup(dri.page_source, 'html.parser')

        if seleniumUtil.checkSoupHaveNoInformationChi(soup):
            if seleniumUtil.refreshForNoInformationChi(dri.current_url, dri):
                debugLogUtil.updateHorseDebugLog(horseId, "HORSE CHINESE NAME HAVE NO INFORMATION")
                return
            else:
                soup = BeautifulSoup(dri.page_source, 'html.parser')

        chineseName = soup.select(horse_id_and_name_selector_1)[0].text
        chineseName = chineseName.split(" (")[0]
        insertIntoInitHorseTable(horseTable, horseId, englishName, trackworkRecordsUrl, veterinaryRecordsUrl, age,
                                 countryOfOrigin, colour, sex,
                                 importType, sire, dam, damSire, chineseName)
        debugLogUtil.updateHorseScrappingLog(logId, horseId, "COMPLETED", None, None)
    except Exception as e:
        debugLogUtil.updateHorseScrappingLog(logId, horseId, "FAILED", e.args[0], traceback.format_exc())
