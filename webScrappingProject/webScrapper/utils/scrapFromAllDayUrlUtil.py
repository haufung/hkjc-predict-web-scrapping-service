from ..utils import debugLogUtil, raceUrlUtil, seleniumUtil, scrapRaceUtil


def validateRaceIdFromDBandLink(url, raceIddb, raceId):
    if raceId != raceIddb:
        debugLogUtil.updateRaceDebugLog(url, "RACE_ID VALIDATION FAILED", raceId, raceIddb, "")
        return False
    else:
        return True


def scrapAllThisRaceUrlFromAllRaceUrl(dri, thisRaceUrlList, raceTable, recordsTable, allRaceUrlTable):
    for raceTuple in thisRaceUrlList:
        raceIdFromDb = raceTuple[0]
        url = raceTuple[1]
        dri.get(url)
        seleniumUtil.checkLoading(dri)
        driverUrl = dri.current_url
        raceId, _ = raceUrlUtil.generateRaceIdFromUrl(driverUrl)
        if validateRaceIdFromDBandLink(url, raceIdFromDb, raceId):
            headerListMapping = scrapRaceUtil.queryInitRecordsHeaderMapping()
            scrapRaceUtil.scrapThisRaceUrl(dri, headerListMapping, driverUrl, None, None, None,
                                           None, raceTable, recordsTable, allRaceUrlTable)
