from urllib.parse import urlparse, parse_qs


def generateRaceIdFromUrl(url):
    o = urlparse(url)
    query = parse_qs(o.query)
    raceId = None
    raceIdNoRaceNumber = None
    try:
        raceNumberInt = int(query['RaceNo'][0])
        if raceNumberInt < 10:
            raceNumberStr = "0" + str(raceNumberInt)
        else:
            raceNumberStr = str(raceNumberInt)
        raceId = query['RaceDate'][0].replace("/", "") + "_" + raceNumberStr
    except Exception as e:
        print(f"Error on querying {url} : {str(e)}")
        raceIdNoRaceNumber = query['RaceDate'][0].replace("/", "")
    return raceId, raceIdNoRaceNumber

def generateLocationFromUrl(url):
    o = urlparse(url)
    query = parse_qs(o.query)
    location = None
    try:
        location = int(query['venue'][0])
    except Exception as e:
        print(f"Error on querying {url} : {str(e)}")
    return location