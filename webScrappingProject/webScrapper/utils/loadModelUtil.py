import math

import tensorflow as tf
from django.db import connection
from tensorflow import feature_column
from tensorflow import keras

from . import modelRequiredHeaderUtil
from ..utils import picklistValueUtil

cur = connection.cursor()


def updatePredictedWin(recordsTable, fieldName, predictionFieldName):
    sqlQuery = rf'''update {recordsTable} set {fieldName} = true from (select max({predictionFieldName}) as highest_prediction, race_id from {recordsTable} where {predictionFieldName} is not null group by race_id) as alias 
where alias.race_id = {recordsTable}.race_id and alias.highest_prediction = {recordsTable}.{predictionFieldName};'''
    cur.execute(sqlQuery)


def updatePredictionInDb(recordsTable, records_id, prediction, fieldName):
    sqlQuery = rf'''update {recordsTable} set {fieldName} = %s where records_id = %s;'''
    cur.execute(sqlQuery, [prediction, records_id])


def df_to_dataset(dataframe, batch_size, shuffle=True):
    dataframe = dataframe.copy()
    _ = dataframe.pop('records_id')
    labels = dataframe.pop('label')
    ds = tf.data.Dataset.from_tensor_slices((dict(dataframe), labels))
    if shuffle:
        ds = ds.shuffle(buffer_size=len(dataframe))
    if batch_size:
        ds = ds.batch(batch_size)
    return ds, labels


def getFeatureColumn(modelVersion, isPredict=None):
    raceLocationPicklist = picklistValueUtil.raceLocationPicklist()
    raceDistancePicklist = picklistValueUtil.raceDistancePicklist()
    raceGoingPicklist = picklistValueUtil.raceGoingPicklist()
    raceCoursePicklist = picklistValueUtil.raceCoursePicklist()
    recordsDrawPicklist = picklistValueUtil.recordsDrawPicklist()
    recordsLastPlaceOnSameCoursePicklist = picklistValueUtil.recordsLastPlaceOnSameCoursePicklist()

    feature_column_header = modelRequiredHeaderUtil.featureColumnHeader(modelVersion)
    feature_column_header_for_prediction = modelRequiredHeaderUtil.featureColumnHeaderForPrediction(modelVersion)
    featureColumns = []
    if isPredict is True:
        for header in feature_column_header_for_prediction:
            featureColumns.append(feature_column.numeric_column(header))
    else:
        for header in feature_column_header:
            featureColumns.append(feature_column.numeric_column(header))
    horseDaysFromFirstRaceBuckets = feature_column.bucketized_column(
        feature_column.numeric_column('horse_days_from_first_race'),
        boundaries=[0, 365, 730, 1095, 1460, 1825, 2190, 2555, 2920, 3285])
    featureColumns.append(horseDaysFromFirstRaceBuckets)
    locationVocabList = feature_column.categorical_column_with_vocabulary_list(
        'location', raceLocationPicklist)
    location = feature_column.indicator_column(locationVocabList)
    featureColumns.append(location)
    distance = feature_column.indicator_column(feature_column.categorical_column_with_vocabulary_list(
        'distance', raceDistancePicklist))
    featureColumns.append(distance)
    going = feature_column.indicator_column(feature_column.categorical_column_with_vocabulary_list(
        'going', raceGoingPicklist))
    featureColumns.append(going)
    courseVocabList = feature_column.categorical_column_with_vocabulary_list(
        'course', raceCoursePicklist)
    course = feature_column.indicator_column(courseVocabList)
    featureColumns.append(course)
    drawVocabList = feature_column.categorical_column_with_vocabulary_list(
        'draw', recordsDrawPicklist)
    draw = feature_column.indicator_column(drawVocabList)
    featureColumns.append(draw)
    recordsFirstLastPlaceOnSameCourse = feature_column.indicator_column(
        feature_column.categorical_column_with_vocabulary_list(
            'first_last_place_on_same_course', recordsLastPlaceOnSameCoursePicklist))
    featureColumns.append(recordsFirstLastPlaceOnSameCourse)
    recordsSecondLastPlaceOnSameCourse = feature_column.indicator_column(
        feature_column.categorical_column_with_vocabulary_list(
            'second_last_place_on_same_course', recordsLastPlaceOnSameCoursePicklist))
    featureColumns.append(recordsSecondLastPlaceOnSameCourse)
    recordsThirdLastPlaceOnSameCourse = feature_column.indicator_column(
        feature_column.categorical_column_with_vocabulary_list(
            'third_last_place_on_same_course', recordsLastPlaceOnSameCoursePicklist))
    featureColumns.append(recordsThirdLastPlaceOnSameCourse)
    categorySize = len(raceLocationPicklist) * len(recordsDrawPicklist) * len(raceCoursePicklist)
    # https: // developers.googleblog.com / 2017 / 11 / introducing - tensorflow - feature - columns.html
    dimension = math.ceil(categorySize ** 0.25)
    location_x_draw_x_course = feature_column.embedding_column(
        feature_column.crossed_column([locationVocabList, drawVocabList, courseVocabList], categorySize), dimension)
    featureColumns.append(location_x_draw_x_course)

    return featureColumns


def makeModel(featureColumnList, output_bias=None):
    featureLayer = tf.keras.layers.DenseFeatures(featureColumnList)
    if output_bias is not None:
        output_bias = tf.keras.initializers.Constant(output_bias)
    model = tf.keras.Sequential([
        featureLayer,
        keras.layers.Dense(100, activation='relu'),
        keras.layers.Dropout(0.2),
        keras.layers.Dense(50, activation='relu'),
        keras.layers.Dropout(0.2),
        keras.layers.Dense(1, activation='sigmoid', bias_initializer=output_bias)
    ])
    return model


def loadModelAndPredictDf(df, tableName, modelDirectory, modelVersion, clearAllField=False):
    predict_ds, labels = df_to_dataset(df, len(df), shuffle=False)
    featureColumnList = getFeatureColumn(modelVersion, isPredict=True)
    new_model = makeModel(featureColumnList)
    new_model.load_weights(modelDirectory)
    predictions = new_model.predict(predict_ds)
    # accuracy = tf.keras.metrics.BinaryAccuracy(name='accuracy', threshold=0.5)
    # print(accuracy(labels, predictions))
    df['predictions'] = predictions
    predictionFieldName = f'prediction_{modelVersion.replace(".", "_")}'
    predictedWinFieldName = f'predicted_win_{modelVersion.replace(".", "_")}'
    cur.execute(rf'''ALTER TABLE {tableName} ADD COLUMN IF NOT EXISTS {predictionFieldName} float;''')
    cur.execute(rf'''ALTER TABLE {tableName} ADD COLUMN IF NOT EXISTS {predictedWinFieldName} boolean;''')
    if clearAllField is True:
        cur.execute(rf'''update {tableName} set {predictionFieldName} = null;''')
        cur.execute(rf'''update {tableName} set {predictedWinFieldName} = null;''')
        connection.commit()
    print(f"Updating {predictionFieldName} ...")
    for index, row in df.iterrows():
        updatePredictionInDb(tableName, row['records_id'], row['predictions'], predictionFieldName)
    print(f"Updating {predictedWinFieldName} ...")
    updatePredictedWin(tableName, predictedWinFieldName, predictionFieldName)
    connection.commit()
