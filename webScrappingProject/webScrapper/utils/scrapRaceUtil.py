import datetime
import re
import time
import traceback

from bs4 import BeautifulSoup

from ..utils import seleniumUtil
from ..utils.debugLogUtil import *

cur = connection.cursor()

# Deprecated
NO_OF_PAGE_TO_LOAD_CSS_SELECTOR = "body > div > div.f_clear.top_races > table > tbody > tr:nth-child(1) > td:not([class]) img"
LOCATION_AND_DATE_CSS_SELECTOR = "body > div > div.raceMeeting_select > p:nth-child(1) > span.f_fl.f_fs13"
RACE_NUMBER_AND_RACE_ID_CSS_SELECTOR = "body > div > div.race_tab > table > thead > tr > td:nth-child(1)"
CLS_GRP_AND_DISTANCE_CSS_SELECTOR = "body > div > div.race_tab > table > tbody > tr:nth-child(2) > td:nth-child(1)"
RACE_NAME_CSS_SELECTOR = "body > div > div.race_tab > table > tbody > tr:nth-child(3) > td:nth-child(1)"
GOING_CSS_SELECTOR = "body > div > div.race_tab > table > tbody > tr:nth-child(2) > td:nth-child(3)"
COURSE_CSS_SELECTOR = "body > div > div.race_tab > table > tbody > tr:nth-child(3) > td:nth-child(3)"
POOL_CSS_SELECTOR = "body > div > div.race_tab > table > tbody > tr:nth-child(4) > td:nth-child(1)"


NO_OF_PAGE_TO_LOAD_CSS_SELECTOR_1 = "#innerContent > div.localResults.commContent > div.f_clear.top_races > table > tbody > tr:nth-child(1) > td:not([class]) img"
LOCATION_AND_DATE_CSS_SELECTOR_1 = "#innerContent > div.localResults.commContent > div.raceMeeting_select > p > span.f_fl.f_fs13"
RACE_NUMBER_AND_RACE_ID_CSS_SELECTOR_1 = "#innerContent > div.localResults.commContent > div.race_tab > table > thead > tr > td:nth-child(1)"
CLS_GRP_AND_DISTANCE_CSS_SELECTOR_1 = "#innerContent > div.localResults.commContent > div.race_tab > table > tbody > tr:nth-child(2) > td:nth-child(1)"
RACE_NAME_CSS_SELECTOR_1 = "#innerContent > div.localResults.commContent > div.race_tab > table > tbody > tr:nth-child(3) > td:nth-child(1)"
GOING_CSS_SELECTOR_1 = "#innerContent > div.localResults.commContent > div.race_tab > table > tbody > tr:nth-child(2) > td:nth-child(3)"
COURSE_CSS_SELECTOR_1 = "#innerContent > div.localResults.commContent > div.race_tab > table > tbody > tr:nth-child(3) > td:nth-child(3)"
POOL_CSS_SELECTOR_1 = "#innerContent > div.localResults.commContent > div.race_tab > table > tbody > tr:nth-child(4) > td:nth-child(1)"
RECORDS_TABLE_HEADER_CSS_SELECTOR = "#innerContent > div.localResults.commContent > div.performance > table > thead > tr > td"
RECORDS_TABLE_ROW_CSS_SELECTOR = "#innerContent > div.localResults.commContent > div.performance > table > tbody > tr"
OPERATION = "INIT SCRAPING"


def queryInitRecordsHeaderMapping():
    cur.execute('select * from init.hkjc_web_records_mapping')
    return dict(cur.fetchall())


def scrapLocation(locationString, scrapUrl):
    loc = ""
    if "Sha Tin" in locationString:
        loc = "ST"
    elif "Happy Valley" in locationString:
        loc = "HV"
    elif "Conghua" in locationString:
        loc = "CH"
    else:
        updateRaceDebugLog(scrapUrl, "ERROR IN RACE LOCATION", loc, "first race url", OPERATION)
    return loc


def generateAllRaceUrlFromInit(url, loc, num):
    link = []
    for j in range(num):
        tmp = url + "/" + loc + "/" + str(j + 1)
        link.append(tmp)
    return link


def generateAllRaceUrlWithRequestParam(url, loc, num):
    link = []
    for j in range(num):
        tmp = url + "&Racecourse=" + loc + "&RaceNo=" + str(j + 1)
        link.append(tmp)
    return link


def scrapRaceDate(sou):
    locationAndDateString = sou.select(LOCATION_AND_DATE_CSS_SELECTOR_1)[0].text
    matchDate = re.search('\d{2}/\d{2}/\d{4}', locationAndDateString)
    date = datetime.datetime.strptime(matchDate.group(), '%d/%m/%Y').date()
    return date

# Deprecated
def scrapRaceNumber(sou):
    stringList = sou.select(RACE_NUMBER_AND_RACE_ID_CSS_SELECTOR_1)[0].text
    stringToReturn = ""
    for k in range(len(stringList)):
        if stringList[k] == " " and stringList[k + 2] == " ":
            stringToReturn = int(stringList[k + 1])
        elif stringList[k] == " " and stringList[k + 3] == " ":
            stringToReturn = int(stringList[k + 1]) * 10 + int(stringList[k + 2])
    return stringToReturn


def validateRaceNumberAndRaceDate(url, dateA, dateB, raceNumA, raceNumB):
    if dateA != dateB:
        updateRaceDebugLog(url, "DATE SCRAPED IS UNEXPECTED", "_".join([str(dateA), str(dateB)]), "", OPERATION)
        print("DATE SCRAPED IS UNEXPECTED")
    if raceNumA != raceNumB:
        updateRaceDebugLog(url, "RACE NUM SCRAPED IS UNEXPECTED", "_".join([raceNumA, raceNumB]), "", OPERATION)
        print("RACE NUM SCRAPED IS UNEXPECTED")


def generateUniqueRaceId(date, raceNum):
    dateString = date.strftime("%Y%m%d")
    listToBeJoined = [dateString]
    if len(str(raceNum)) == 1:  # raceNumber 0 - 9
        listToBeJoined.append("0" + str(raceNum))
    elif len(str(raceNum)) > 1:  # raceNumber > 9
        listToBeJoined.append(str(raceNum))
    return "_".join(listToBeJoined)


def scrapClassDistanceRating(raceId, url, sou):
    soupString = sou.select(CLS_GRP_AND_DISTANCE_CSS_SELECTOR_1)[0].contents[0]
    stringList = soupString.split(" - ")
    if len(stringList) == 1:
        updateRaceDebugLog(url, "SOMETHING WRONG WITH CLASS / DISTANCE / RATING", soupString, raceId, OPERATION)
        return "", "", ""
    elif len(stringList) == 1:
        return stringList[0], "", ""
    elif len(stringList) == 2:
        return stringList[0], stringList[1], ""
    elif len(stringList) == 3:
        return stringList[0], stringList[1], stringList[2]
    elif len(stringList) > 3:
        updateRaceDebugLog(url, "SOMETHING WRONG WITH CLASS / DISTANCE / RATING", soupString, raceId, OPERATION)
        return stringList[0], stringList[1], stringList[2]


def insertIntoInitRaceTable(raceTable, race_id, race_date, location, race_number, race_class, distance, going, course,
                            pool,
                            race_name_english, rating):
    cur.execute(rf'''INSERT INTO {raceTable}
    ("race_id",
"race_date",
"location",
"race_number",
"class",
"distance",
"going",
"course",
"pool",
"race_name_english",
"rating")
    VALUES
    (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)''',
                [race_id, race_date, location, race_number, race_class, distance, going, course, pool,
                 race_name_english,
                 rating])


def insertIntoInitAllRaceUrlTable(allRaceUrlTable, raceId, url):
    cur.execute(rf'''INSERT INTO {allRaceUrlTable}
        ("race_id",
        "url")
        VALUES
        (%s,%s) ON CONFLICT (race_id) DO UPDATE 
              SET race_id = %s, 
                    url = %s,
                    have_refunded = null,
                    no_data = null;''', [raceId, url, raceId, url])


def insertIntoInitAllRaceUrlTableWithRefundFlag(allRaceUrlTable, raceId, url):
    cur.execute(rf'''INSERT INTO {allRaceUrlTable}
        ("race_id",
        "url",
        "have_refunded")
        VALUES
        (%s,%s, true) ON CONFLICT (race_id) DO UPDATE 
              SET race_id = %s, 
                    url = %s,
                    have_refunded = true,
                    no_data = null;''', [raceId, url, raceId, url])
    connection.commit()


def insertIntoInitAllRaceUrlTableWithNoDataFlag(allRaceUrlTable, raceId, url):
    cur.execute(rf'''INSERT INTO {allRaceUrlTable}
        ("race_id",
        "url",
        "no_data")
        VALUES
        (%s,%s, true) ON CONFLICT (race_id) DO UPDATE 
              SET race_id = %s, 
                    url = %s,
                    have_refunded = null,
                    no_data = true;''', [raceId, url, raceId, url])
    connection.commit()


def checkSoupHaveRefund(allRaceUrlTable, raceId, sou, dri):
    if "REFUND" in sou.get_text():
        updateRaceDebugLog(dri.current_url, "RACE HAVE REFUNDED", sou.get_text(), "", OPERATION)
        insertIntoInitAllRaceUrlTableWithRefundFlag(allRaceUrlTable, raceId, dri.current_url)
        return True
    else:
        return False


def driverGetCatchException(url, dri, operation=OPERATION):
    startTime = time.time()
    try:
        dri.get(url)
    except Exception as e:
        errorTime = time.time()
        updateRaceDebugLog(url, str(e), str(int(errorTime - startTime)), "", operation)
        dri = seleniumUtil.rotateUserAgent(dri)
        dri.get(url)
    return dri


def scrapRecordsTableHeader(sou):
    headerList = []
    headerLength = len(sou.select(RECORDS_TABLE_HEADER_CSS_SELECTOR))
    for x in range(headerLength):
        header = sou.select(RECORDS_TABLE_HEADER_CSS_SELECTOR + f':nth-child({x + 1})')[0].text
        headerList.append(header)
    return headerList


def insertIntoInitRecordsTable(recordsTable, columnNameList, resultList, race_id, horse_id, records_date):
    dbColumn = '"' + ('","').join(columnNameList) + '"'
    placeHolder = ['%s,'] * len(columnNameList)
    resultList.append(race_id + "_" + horse_id)
    resultList.append(records_date)
    resultList.append(race_id)
    cur.execute(rf'''INSERT INTO {recordsTable}
                ({dbColumn},"records_id", "records_date", "race_id")
                VALUES
                ({"".join(placeHolder)}%s,%s,%s)''',
                resultList)


def scrapRecordsTable(sou, webHeaderList, headerMap, raceId, recordDate, recordsTable):
    lengthOfRecords = len(sou.select(RECORDS_TABLE_ROW_CSS_SELECTOR))
    for rowNum in range(lengthOfRecords):
        listOfRowResult = []
        dbColumnNameList = []
        horseId = ""
        this_css_selector = RECORDS_TABLE_ROW_CSS_SELECTOR + f':nth-child({rowNum + 1})' + f' > td'
        thisRowSoup = sou.select(this_css_selector)
        for index, webHeader in enumerate(webHeaderList):
            exec(headerMap[webHeader] + f' = thisRowSoup[{index}].text')
            dbColumnNameList.append(headerMap[webHeader])
            if headerMap[webHeader] == 'horse_number':
                try:
                    exec(f'{headerMap[webHeader]} = re.findall(r"\d+", {headerMap[webHeader]})[0]')
                except Exception as e:
                    exec(f'{headerMap[webHeader]} = ""')
            elif headerMap[webHeader] == 'horse_id':
                exec(f'{headerMap[webHeader]} = re.findall(r"\(([A-Za-z0-9_]+)\)", {headerMap[webHeader]})[0]')
                horseId = eval(headerMap[webHeader])
            elif headerMap[webHeader] == 'jockey_name' or headerMap[webHeader] == 'trainer_name':
                exec(f'''{headerMap[webHeader]} = " ".join(re.findall(r"\w+|'|-", {headerMap[webHeader]}))''')
            elif headerMap[webHeader] == 'running_position':
                exec(f'''{headerMap[webHeader]} = " ".join(re.findall(r"\d+|-", {headerMap[webHeader]}))''')
            elif headerMap[webHeader] == 'place':
                exec(f'''{headerMap[webHeader]} = {headerMap[webHeader]}.replace("\xa0", "")''')
            listOfRowResult.append(eval(headerMap[webHeader]))
        insertIntoInitRecordsTable(recordsTable, dbColumnNameList, listOfRowResult, raceId, horseId, recordDate)


def onlyCheckIfItIsRefunded(dri, thisRaceUrl, dateToValidate, raceNumberToValidate, forceNoRefund):
    dri = driverGetCatchException(thisRaceUrl, dri)
    soup = checkingAfterEnteringUrl(thisRaceUrl, dri, "this race url", OPERATION)
    if soup is False:
        return
    scrapedRaceDate = scrapRaceDate(soup)
    scrapedRaceNumber = thisRaceUrl.split("/")[-1]

    if dateToValidate is not None and raceNumberToValidate is not None:
        validateRaceNumberAndRaceDate(dri.current_url, dateToValidate, scrapedRaceDate, raceNumberToValidate,
                                      scrapedRaceNumber)

    raceUniqueId = generateUniqueRaceId(scrapedRaceDate, scrapedRaceNumber)

    if forceNoRefund is None and checkSoupHaveRefund(raceUniqueId, soup, dri):
        return


def scrapThisRaceUrl(dri, headerListMapping, thisRaceUrl, location, dateToValidate, raceNumberToValidate, forceNoRefund,
                     raceTable, recordsTable, allRaceUrlTable):
    dri = driverGetCatchException(thisRaceUrl, dri)
    soup = checkingAfterEnteringUrl(thisRaceUrl, dri, "this race url", OPERATION)
    if soup is False:
        return
    scrapedRaceDate = scrapRaceDate(soup)
    scrapedRaceNumber = thisRaceUrl.split("/")[-1]

    if dateToValidate is not None and raceNumberToValidate is not None:
        validateRaceNumberAndRaceDate(dri.current_url, dateToValidate, scrapedRaceDate, raceNumberToValidate,
                                      scrapedRaceNumber)

    raceUniqueId = generateUniqueRaceId(scrapedRaceDate, scrapedRaceNumber)

    if forceNoRefund is None and checkSoupHaveRefund(allRaceUrlTable, raceUniqueId, soup, dri):
        return

    logId = insertRaceScrappingLog(raceUniqueId)
    try:
        raceClass, distance, rating = scrapClassDistanceRating(raceUniqueId, dri.current_url, soup)
        distance = distance.replace(" ", "")
        raceName = soup.select(RACE_NAME_CSS_SELECTOR_1)[0].text
        going = soup.select(GOING_CSS_SELECTOR_1)[0].text
        course = soup.select(COURSE_CSS_SELECTOR_1)[0].text
        pool = soup.select(POOL_CSS_SELECTOR_1)[0].text
        if location is None:
            locationString = soup.select(LOCATION_AND_DATE_CSS_SELECTOR_1)[0].text
            location = scrapLocation(locationString, dri.current_url)
        recordsTableHeader = scrapRecordsTableHeader(soup)
        if len(recordsTableHeader) > 0:
            scrapRecordsTable(soup, recordsTableHeader, headerListMapping, raceUniqueId, scrapedRaceDate, recordsTable)
            insertIntoInitRaceTable(raceTable, raceUniqueId, scrapedRaceDate, location, scrapedRaceNumber,
                                    raceClass, distance, going, course, pool, raceName, rating)
            insertIntoInitAllRaceUrlTable(allRaceUrlTable, raceUniqueId, dri.current_url)
        else:
            insertIntoInitAllRaceUrlTableWithNoDataFlag(allRaceUrlTable, raceUniqueId, dri.current_url)
        connection.commit()
        updateRaceScrappingLog(logId, "COMPLETED", None, None)
    except Exception as e:
        updateRaceScrappingLog(logId, "FAILED", e.args[0], traceback.format_exc())


def checkingAfterEnteringUrl(url, dri, reference, operation):
    seleniumUtil.checkLoading(dri)

    soup = BeautifulSoup(dri.page_source, 'html.parser')
    if seleniumUtil.checkSoupHaveNoInformation(soup):
        if seleniumUtil.refreshForNoInformation(url, dri):
            updateRaceDebugLog(dri.current_url, "RACE HAVE NO INFORMATION", soup.get_text(), reference, operation)
            return False
        else:
            soup = BeautifulSoup(dri.page_source, 'html.parser')

    if seleniumUtil.checkSoupHaveCookieChecking(soup):
        if seleniumUtil.refreshForCookieChecking(url, dri):
            updateRaceDebugLog(dri.current_url, "RACE HAVE COOKIE CHECKING", soup.get_text(), reference, operation)
            return False
        else:
            soup = BeautifulSoup(dri.page_source, 'html.parser')

    if seleniumUtil.checkSoupHaveNoInformationChi(soup):
        if seleniumUtil.refreshForNoInformationChi(dri.current_url, dri):
            updateRaceDebugLog(dri.current_url, "RACE CHINESE HAVE NO INFORMATION", soup.get_text(), reference,
                               operation)
            return False
        else:
            soup = BeautifulSoup(dri.page_source, 'html.parser')
    return soup


def generateAllRaceUrl(url, dri, debugMode):
    dri = driverGetCatchException(url, dri)
    soup = checkingAfterEnteringUrl(url, dri, "first race url", OPERATION)
    if soup is False:
        return
    soup2 = soup.select(NO_OF_PAGE_TO_LOAD_CSS_SELECTOR_1)
    numberOfRace = len(soup2) - 1
    locationString = soup.select(LOCATION_AND_DATE_CSS_SELECTOR_1)[0].text
    location = scrapLocation(locationString, dri.current_url)
    if debugMode is None:
        raceUrls = generateAllRaceUrlFromInit(url, location, numberOfRace)
    else:
        raceUrls = generateAllRaceUrlWithRequestParam(url, location, numberOfRace)

    return raceUrls, location


def scrapFirstRaceUrl(dri, headerListMapping, raceDate, firstRaceUrl, debugMode, onlyCheckRefund, raceTable,
                      recordsTable, allRaceUrlTable):
    raceUrls, location = generateAllRaceUrl(firstRaceUrl, dri, debugMode)
    for raceNumberFrom0 in range(len(raceUrls)):
        thisRaceUrl = raceUrls[raceNumberFrom0]
        if onlyCheckRefund is None:
            scrapThisRaceUrl(dri, headerListMapping, thisRaceUrl, location, raceDate, str(raceNumberFrom0 + 1), None,
                             raceTable, recordsTable, allRaceUrlTable)
        else:
            onlyCheckIfItIsRefunded(dri, thisRaceUrl, None, None, None)


def queryAllRaceDayUrlToScrap():
    cur.execute(f'''select * from init.all_race_day order by race_date desc''')
    return cur.fetchall()


def queryAllRaceDayUrlToScrapAutoUpdate(raceTable):
    cur.execute(
        f'''select * from init.all_race_day where race_date > (select race_date from {raceTable} order by race_date desc limit 1) and is_oversea is null order by race_date asc''')
    return cur.fetchall()
