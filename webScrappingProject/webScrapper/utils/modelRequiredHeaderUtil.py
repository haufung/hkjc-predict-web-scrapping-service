from django.db import connection

cur = connection.cursor()

DB_TO_STORE_REQUIRED_HEADER = 'config'


def raceHeader(version):
    cur.execute(rf''' select race_header from {DB_TO_STORE_REQUIRED_HEADER}.model_header_required where version = %s and race_header is not null;''', [version])
    raceHeaderRequired = [r[0] for r in cur.fetchall()]
    return raceHeaderRequired

def recordsHeader(version):
    cur.execute(rf''' select records_header from {DB_TO_STORE_REQUIRED_HEADER}.model_header_required where version = %s and records_header is not null;''', [version])
    recordsHeaderRequired = [r[0] for r in cur.fetchall()]
    return recordsHeaderRequired

def recordsHeaderForPrediction(version):
    cur.execute(rf''' select records_header_for_prediction from {DB_TO_STORE_REQUIRED_HEADER}.model_header_required where version = %s and records_header_for_prediction is not null;''', [version])
    recordsHeaderRequiredForPrediction = [r[0] for r in cur.fetchall()]
    return recordsHeaderRequiredForPrediction

def featureColumnHeader(version):
    cur.execute(rf''' select feature_column_header from {DB_TO_STORE_REQUIRED_HEADER}.model_header_required where version = %s and feature_column_header is not null;''', [version])
    featureColumnHeaderRequired = [r[0] for r in cur.fetchall()]
    return featureColumnHeaderRequired

def featureColumnHeaderForPrediction(version):
    cur.execute(rf''' select feature_column_header_for_prediction from {DB_TO_STORE_REQUIRED_HEADER}.model_header_required where version = %s and feature_column_header_for_prediction is not null;''', [version])
    featureColumnHeaderRequiredForPrediction = [r[0] for r in cur.fetchall()]
    return featureColumnHeaderRequiredForPrediction
