import datetime
from urllib import parse

def parseRaceUrlAndGenerateSectionalTimeUrl(raceUrl):
    raceDateParam = parse.parse_qs(parse.urlparse(raceUrl).query)['RaceDate'][0]
    raceNoParam = parse.parse_qs(parse.urlparse(raceUrl).query)['RaceNo'][0]

    newRaceDateParam = datetime.datetime.strptime(raceDateParam, '%Y/%m/%d').strftime('%d/%m/%Y')
    return 'https://racing.hkjc.com/racing/information/English/Racing/DisplaySectionalTime.aspx?RaceDate=' + newRaceDateParam + '&RaceNo=' + raceNoParam
