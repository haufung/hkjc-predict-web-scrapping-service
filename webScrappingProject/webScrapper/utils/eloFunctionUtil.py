from django.db import connection
cur = connection.cursor()


def declareFindHorseOrJockeyOrTrainerEloFromLastRaceFunction(fromRecordsTable, columnName, fieldName):
    cur.execute(rf'''DROP FUNCTION IF EXISTS {fieldName}_last_race(text,text);
                CREATE OR REPLACE FUNCTION {fieldName}_last_race(this_{columnName} text, this_records_id text)
                RETURNS float AS $$
                declare
                    {fieldName}_last_race text;
                BEGIN

                SELECT
                coalesce(
                (SELECT 
                {fieldName}_after from {fromRecordsTable} where {columnName} = this_{columnName}
                 and records_id < this_records_id and draw_is_null is not true
                 order by records_id desc limit 1),1500)
                   into {fieldName}_last_race;
                   RETURN {fieldName}_last_race;
                END;
                $$ LANGUAGE plpgsql;''')

def declareHorseOrJockeyOrTrainerEloScoreFunction(fromRecordsTable, columnName, fieldName):
    cur.execute(rf'''DROP FUNCTION IF EXISTS score_function_for_{fieldName}(text,text);
    CREATE OR REPLACE FUNCTION score_function_for_{fieldName}(this_{columnName} text, this_race_id text)
    RETURNS float AS 
    $$
    declare 
        sf double precision;
        nw_hr_num integer := (select coalesce((select count(race_id)/2 from {fromRecordsTable} where place like '% DH' and race_id = this_race_id group by race_id), 0))::integer;
        n integer := (select count(race_id) from {fromRecordsTable} where race_id = this_race_id group by race_id) - nw_hr_num;
    BEGIN

     select case when place like '% DH'
     then (n - (select coalesce(substring(place from '(..) DH'), substring(place from '(.) DH')))::float+0.5)/((n*(n-1))/2)
     else (n - place::float)/((n*(n-1))/2)
     end as sf from {fromRecordsTable} where race_id = this_race_id and {columnName} = this_{columnName} and place not in (select abbreviation from config.place_special_incidents_index)
       into sf;
       RETURN sf;
    END;
    $$ 
    LANGUAGE plpgsql;''')

def declareHorseOrJockeyOrTrainerEloEstimateScoreFunction(toRecordsTable, columnName, fieldName):
    cur.execute(rf'''DROP FUNCTION IF EXISTS estimate_score_function_for_{fieldName}(text,text,float);
    CREATE OR REPLACE FUNCTION estimate_score_function_for_{fieldName}(this_{columnName} text, this_race_id text , this_{fieldName}_before float)
    RETURNS float AS $$
    declare
        esf float;
        race_total_number float := (select count(race_id)::integer from {toRecordsTable} where race_id = this_race_id group by race_id)::float;
    BEGIN

    select alias.nom/((race_total_number*(race_total_number - 1))/2) as esf from (
    select sum(1/(1+ "power10"({fieldName}_before,this_{fieldName}_before))) as nom from {toRecordsTable} where 
    race_id = this_race_id and
    {columnName} != this_{columnName} 
    group by race_id) as alias
       into esf;
       RETURN esf;
       exception when numeric_value_out_of_range then 
            raise exception 'Value out of range for hor_id = % rid= % this_{fieldName}_before= % r_hr_num= %  esf= %', this_{columnName}, this_race_id, {fieldName}_before, race_total_number, esf;
    END;
    $$ LANGUAGE plpgsql;''')

def declareHorseOrJockeyOrTrainerEloFunction(toRecordsTable, columnName, fieldName):
    cur.execute(rf'''CREATE OR REPLACE FUNCTION compute_{fieldName}_with_k(nk integer, this_race_id text)
        RETURNS void AS $$

        BEGIN 
        update {toRecordsTable}  
        set {fieldName}_before = alias.{fieldName}_last_race
        from (select "{fieldName}_last_race"({columnName}, records_id), records_id from {toRecordsTable} where race_id = this_race_id) as alias
        where alias.records_id = {toRecordsTable}.records_id;

        update {toRecordsTable} 
        set {fieldName}_after = alias3.{fieldName}_before::float + nk*(alias2.sf::float - alias1.esf::float)
        from
        (select "estimate_score_function_for_{fieldName}"({columnName}, race_id, {fieldName}_before) as esf, records_id from {toRecordsTable} where race_id = this_race_id) as alias1,
        (select "score_function_for_{fieldName}"({columnName}, race_id) as sf, records_id from {toRecordsTable} where race_id = this_race_id) as alias2,
        (select {fieldName}_before, records_id from {toRecordsTable} where race_id = this_race_id) as alias3
        where {toRecordsTable}.records_id = alias1.records_id 
        and {toRecordsTable}.records_id = alias2.records_id 
        and {toRecordsTable}.records_id = alias3.records_id;
        END;
        $$ LANGUAGE plpgsql;''')

def declareFindHorseOrJockeyOrTrainerEloFromLastRaceByDateFunction(fromRecordsTable, columnName, fieldName):
    cur.execute(rf'''DROP FUNCTION IF EXISTS {fieldName}_last_race_by_date(text,date);
                CREATE OR REPLACE FUNCTION {fieldName}_last_race_by_date(this_{columnName} text, this_records_date date)
                RETURNS float AS $$
                declare
                    {fieldName}_last_race_by_date text;
                BEGIN

                SELECT
                coalesce(
                (SELECT 
                {fieldName}_after from {fromRecordsTable} where {columnName} = this_{columnName}
                 and records_date < this_records_date and draw_is_null is not true
                 order by race_id desc limit 1),1500)
                   into {fieldName}_last_race_by_date;
                   RETURN {fieldName}_last_race_by_date;
                END;
                $$ LANGUAGE plpgsql;''')
