from django.db import connection

cur = connection.cursor()

def normalizeElo(date, eloType, recordsTable):
    cur.execute(rf'''ALTER TABLE {recordsTable} ADD COLUMN IF NOT EXISTS normalized_{eloType}_elo_before float;''')
    cur.execute(rf'''ALTER TABLE {recordsTable} ADD COLUMN IF NOT EXISTS normalized_{eloType}_elo_after float;''')
    cur.execute(rf'''UPDATE {recordsTable} SET normalized_{eloType}_elo_before = alias.{eloType}_elo_before/5000 
                    from (select * from {recordsTable}) as alias where alias.records_id = {recordsTable}.records_id and {recordsTable}.records_date > '{date}';''')
    cur.execute(rf'''UPDATE {recordsTable} SET normalized_{eloType}_elo_after = alias.{eloType}_elo_after/5000 
                        from (select * from {recordsTable}) as alias where alias.records_id = {recordsTable}.records_id and {recordsTable}.records_date > '{date}';''')
    connection.commit()
    print(f"Normalized {eloType} elo!")

def normalizeEloForPrediction(date, eloType, recordsTable):
    cur.execute(rf'''ALTER TABLE {recordsTable} ADD COLUMN IF NOT EXISTS normalized_{eloType}_elo_before_for_prediction float;''')
    cur.execute(rf'''UPDATE {recordsTable} SET normalized_{eloType}_elo_before_for_prediction = alias.{eloType}_elo_before_for_prediction/5000 
                    from (select * from {recordsTable}) as alias where alias.records_id = {recordsTable}.records_id and {recordsTable}.records_date > '{date}';''')
    connection.commit()
    print(f"Normalized {eloType} elo for prediction!")

def normalizeWeightDifferenceFromLastRace(date, recordsTable):
    cur.execute(rf'''ALTER TABLE {recordsTable} ADD COLUMN IF NOT EXISTS normalized_weight_difference_from_last_race float;''')
    cur.execute(rf'''UPDATE {recordsTable} SET normalized_weight_difference_from_last_race = (alias.weight_difference_from_last_race - 1500)/3000 
                    from (select * from {recordsTable}) as alias where alias.records_id = {recordsTable}.records_id and {recordsTable}.records_date > '{date}';''')
    connection.commit()
    print(f"Normalized weight_difference_from_last_race!")

def normalizeDeclareWeight(date, recordsTable):
    cur.execute(rf'''ALTER TABLE {recordsTable} ADD COLUMN IF NOT EXISTS normalized_declare_weight float;''')
    cur.execute(rf'''UPDATE {recordsTable} SET normalized_declare_weight = (alias.declare_weight - 1000)/1000 
                    from (select * from {recordsTable}) as alias where alias.records_id = {recordsTable}.records_id and {recordsTable}.records_date > '{date}';''')
    connection.commit()
    print(f"Normalized declare_weight!")

def normalizeActualWeight(date, recordsTable):
    cur.execute(rf'''ALTER TABLE {recordsTable} ADD COLUMN IF NOT EXISTS normalized_actual_weight float;''')
    cur.execute(rf'''UPDATE {recordsTable} SET normalized_actual_weight = alias.actual_weight/150
                    from (select * from {recordsTable}) as alias where alias.records_id = {recordsTable}.records_id and {recordsTable}.records_date > '{date}';''')
    connection.commit()
    print(f"Normalized actual_weight!")

def normalizeDaysApartFromPreviousRace(date, recordsTable):
    cur.execute(rf'''ALTER TABLE {recordsTable} ADD COLUMN IF NOT EXISTS normalized_days_apart_from_previous_race float;''')
    cur.execute(rf'''UPDATE {recordsTable} SET normalized_days_apart_from_previous_race = alias.days_apart_from_previous_race/1200
                    from (select * from {recordsTable}) as alias where alias.records_id = {recordsTable}.records_id and {recordsTable}.records_date > '{date}';''')
    connection.commit()
    print(f"Normalized days_apart_from_previous_race!")

def normalizeHorseRaceHaveTaken(date, recordsTable):
    cur.execute(rf'''ALTER TABLE {recordsTable} ADD COLUMN IF NOT EXISTS normalized_horse_race_have_taken float;''')
    cur.execute(rf'''UPDATE {recordsTable} SET normalized_horse_race_have_taken = alias.horse_race_have_taken/120
                    from (select * from {recordsTable}) as alias where alias.records_id = {recordsTable}.records_id and {recordsTable}.records_date > '{date}';''')
    connection.commit()
    print(f"Normalized normalized_horse_race_have_taken!")