import traceback

from django.http import JsonResponse

from .controller import scrapRealTimeRace_8, predictRealTimeRace_9, autoUpdate_10, scrapRaceDateController, \
    trainModel_7, calculatedField_5, balanceLabel_6, calculateProfitWithThresholdController, \
    patchingRaceChineseName_2_1, generateRaceLinkFromDate_1, patchingSectionalTimeUrl_2_2
from .utils import configUtil, debugLogUtil

def healthCheck(request):
    return JsonResponse({"code": 200, "message": "UP"})

def scrapRealTimeRace(request):
    logId = debugLogUtil.insertServiceLog(request.path)
    try:
        scrapRealTimeRace_8.main()
    except Exception as e:
        debugLogUtil.updateServiceLog(logId, "FAILED", e.args[0], traceback.format_exc())
        return JsonResponse({"code": 500, "message": e.args[0]}, status=500)
    debugLogUtil.updateServiceLog(logId, "COMPLETED", None, None)
    return JsonResponse({"code": 200, "message": "Success"})


def predictRealTimeRace(request):
    logId = debugLogUtil.insertServiceLog(request.path)
    try:
        predictRealTimeRace_9.main()
    except Exception as e:
        debugLogUtil.updateServiceLog(logId, "FAILED", e.args[0], traceback.format_exc())
        return JsonResponse({"code": 500, "message": e.args[0]}, status=500)
    debugLogUtil.updateServiceLog(logId, "COMPLETED", None, None)
    return JsonResponse({"code": 200, "message": "Success"})


def autoUpdate(request):
    logId = debugLogUtil.insertServiceLog(request.path)
    try:
        autoUpdate_10.main()
    except Exception as e:
        debugLogUtil.updateServiceLog(logId, "FAILED", e.args[0], traceback.format_exc())
        return JsonResponse({"code": 500, "message": e.args[0]}, status=500)
    debugLogUtil.updateServiceLog(logId, "COMPLETED", None, None)
    return JsonResponse({"code": 200, "message": "Success"})

def scrapRaceDate(request):
    logId = debugLogUtil.insertServiceLog(request.path)
    try:
        scrapRaceDateController.startScrap()
    except Exception as e:
        debugLogUtil.updateServiceLog(logId, "FAILED", e.args[0], traceback.format_exc())
        return JsonResponse({"code": 500, "message": e.args[0]}, status=500)
    debugLogUtil.updateServiceLog(logId, "COMPLETED", None, None)
    return JsonResponse({"code": 200, "message": "Success"})

def trainModel(request):
    logId = debugLogUtil.insertServiceLog(request.path)
    try:
        version = request.GET['version']
        if version in configUtil.allVersion():
            trainModel_7.startTrainModel(version)
        else:
            return JsonResponse({"code": 400, "message": "Model Version not supported"})
    except Exception as e:
        debugLogUtil.updateServiceLog(logId, "FAILED", e.args[0], traceback.format_exc())
        return JsonResponse({"code": 500, "message": e.args[0]}, status=500)
    debugLogUtil.updateServiceLog(logId, "COMPLETED", None, None)
    return JsonResponse({"code": 200, "message": "Success"})

def calculatedField(request):
    logId = debugLogUtil.insertServiceLog(request.path)
    try:
        calculatedField_5.main()
    except Exception as e:
        debugLogUtil.updateServiceLog(logId, "FAILED", e.args[0], traceback.format_exc())
        return JsonResponse({"code": 500, "message": e.args[0]}, status=500)
    debugLogUtil.updateServiceLog(logId, "COMPLETED", None, None)
    return JsonResponse({"code": 200, "message": "Success"})


def balanceLabel(request):
    logId = debugLogUtil.insertServiceLog(request.path)
    try:
        balanceLabel_6.main()
    except Exception as e:
        debugLogUtil.updateServiceLog(logId, "FAILED", e.args[0], traceback.format_exc())
        return JsonResponse({"code": 500, "message": e.args[0]}, status=500)
    debugLogUtil.updateServiceLog(logId, "COMPLETED", None, None)
    return JsonResponse({"code": 200, "message": "Success"})


def calculateProfitWithThreshold(request):
    logId = debugLogUtil.insertServiceLog(request.path)
    try:
        version = request.GET['version']
        calculateProfitWithThresholdController.startCalculate(version)
    except Exception as e:
        debugLogUtil.updateServiceLog(logId, "FAILED", e.args[0], traceback.format_exc())
        return JsonResponse({"code": 500, "message": e.args[0]}, status=500)
    debugLogUtil.updateServiceLog(logId, "COMPLETED", None, None)
    return JsonResponse({"code": 200, "message": "Success"})

def updateRaceChineseName(request):
    logId = debugLogUtil.insertServiceLog(request.path)
    try:
        patchingRaceChineseName_2_1.main()
    except Exception as e:
        debugLogUtil.updateServiceLog(logId, "FAILED", e.args[0], traceback.format_exc())
        return JsonResponse({"code": 500, "message": e.args[0]}, status=500)
    debugLogUtil.updateServiceLog(logId, "COMPLETED", None, None)
    return JsonResponse({"code": 200, "message": "Success"})

def updateSectionalTimeUrl(request):
    logId = debugLogUtil.insertServiceLog(request.path)
    try:
        patchingSectionalTimeUrl_2_2.main()
    except Exception as e:
        debugLogUtil.updateServiceLog(logId, "FAILED", e.args[0], traceback.format_exc())
        return JsonResponse({"code": 500, "message": e.args[0]}, status=500)
    debugLogUtil.updateServiceLog(logId, "COMPLETED", None, None)
    return JsonResponse({"code": 200, "message": "Success"})

def generateRaceLinkFromDate(request):
    logId = debugLogUtil.insertServiceLog(request.path)
    try:
        generateRaceLinkFromDate_1.main()
    except Exception as e:
        debugLogUtil.updateServiceLog(logId, "FAILED", e.args[0], traceback.format_exc())
        return JsonResponse({"code": 500, "message": e.args[0]}, status=500)
    debugLogUtil.updateServiceLog(logId, "COMPLETED", None, None)
    return JsonResponse({"code": 200, "message": "Success"})
