from selenium import webdriver

from ..utils import scrapRaceUtil
from ..utils import webDriverUtil
from ..utils.debugLogUtil import *

cur = connection.cursor()

recordsTable = 'init.init_records'
raceTable = 'init.init_race'
allRaceUrlTable = 'init.all_race_url'


def main():
    allRaceUrl = scrapRaceUtil.queryAllRaceDayUrlToScrap()
    headerListMapping = scrapRaceUtil.queryInitRecordsHeaderMapping()
    options, chrome_driver_path = webDriverUtil.webDriverSetup()
    driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)

    for raceTuple in allRaceUrl:
        scrapRaceUtil.scrapFirstRaceUrl(driver, headerListMapping, raceTuple[0], raceTuple[1], None, None, raceTable,
                                        recordsTable, allRaceUrlTable)
    driver.quit()
