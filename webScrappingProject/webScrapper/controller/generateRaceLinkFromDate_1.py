from django.db import connection

cur = connection.cursor()

def main():
    cur.execute(f'''select race_date from init.all_race_day where url is null order by race_date asc''')
    for date in cur.fetchall():
        url = f'http://racing.hkjc.com/racing/info/meeting/Results/English/Local/{date[0].strftime("%Y%m%d")}'
        cur.execute(f"UPDATE init.all_race_day SET url = '{url}' where race_date = '{date[0]}'")
        print(url)
        connection.commit()
