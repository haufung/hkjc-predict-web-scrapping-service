import datetime

from bs4 import BeautifulSoup
from selenium import webdriver

from django.db import connection

from . import patchingSectionalTimeUrl_2_2, scrapHorseData_4
from ..utils import webDriverUtil, seleniumUtil, scrapRaceUtil, dataValidationUtil, \
    calculatedFieldUtil, dataPreprocessUtil, modelDataUtil, loadModelUtil, configUtil, scrapHorseUtil


cur = connection.cursor()

init_web = "https://racing.hkjc.com/racing/information/Chinese/racing/LocalResults.aspx"

def checkIfDateNotExist(date):
    cur.execute('select * from init.all_race_day where race_date = %s', [date])
    row = cur.fetchall()
    if len(row) == 0:
        return True
    else:
        return False

def scrapLatestRaceDate():
    options, chrome_driver_path = webDriverUtil.webDriverSetup()
    driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
    driver.get(init_web)
    seleniumUtil.checkLoading(driver)
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    allDateSoup = soup.select('#selectId > option')
    for dateSoup in allDateSoup:
        date = datetime.datetime.strptime(dateSoup.text, '%d/%m/%Y').date()
        if date >= datetime.datetime.now().date():
            continue
        url = f'http://racing.hkjc.com/racing/info/meeting/Results/English/Local/{date.strftime("%Y%m%d")}'
        if checkIfDateNotExist(date):
            cur.execute(f'INSERT INTO init.all_race_day ("race_date", "url") VALUES (%s,%s)', [date, url])
            connection.commit()
            print(url)
    driver.quit()

def scrapRaceData(allRaceDayTable, allRaceUrlTable, raceTable, recordsTable):
    allRaceUrl = scrapRaceUtil.queryAllRaceDayUrlToScrapAutoUpdate(raceTable)
    headerListMapping = scrapRaceUtil.queryInitRecordsHeaderMapping()
    options, chrome_driver_path = webDriverUtil.webDriverSetup()
    driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)

    for raceTuple in allRaceUrl:
        date = raceTuple[0]
        url = raceTuple[1]
        driver = scrapRaceUtil.driverGetCatchException(url, driver, "AUTO UPDATE")
        if "overseas" in driver.current_url:
            cur.execute(f'''update {allRaceDayTable} set is_oversea = true where race_date = %s ''', [date])
            connection.commit()
            continue
        scrapRaceUtil.scrapFirstRaceUrl(driver, headerListMapping, date, url, None, None, raceTable,
                                        recordsTable, allRaceUrlTable)
    driver.quit()


def insertNewRowToModelTable(fromRaceTable, fromRecordsTable, toRaceTable, toRecordsTable):
    cur.execute(
        rf'''insert into {toRaceTable} select * from {fromRaceTable} where {fromRaceTable}.race_id not in (select race_id from {toRaceTable});''')
    schemaAndTable = fromRecordsTable.split(".")
    cur.execute(
        rf'''SELECT concat('"',column_name,'"')
             FROM information_schema.columns
             WHERE table_schema = '{schemaAndTable[0]}'
             AND table_name = '{schemaAndTable[1]}';''')
    fromTableColumnName = [r[0] for r in cur.fetchall()]
    joinedFromTableColumnName = ",".join(fromTableColumnName)
    cur.execute(
        rf'''insert into {toRecordsTable} ({joinedFromTableColumnName}) select {joinedFromTableColumnName} from {fromRecordsTable} where {fromRecordsTable}.race_id not in (select distinct(race_id) from {toRecordsTable})''')
    connection.commit()

def removePredictValueInRace(raceTable):
    cur.execute(rf'''delete from {raceTable} where is_predict_value = true;''')
    connection.commit()

def initLoadToModelTable():
    initRecordsTable = 'model.init_records'
    initRaceTable = 'model.init_race'
    removePredictValueInRace('model.race')
    insertNewRowToModelTable('init.init_race', 'init.init_records', initRaceTable, initRecordsTable)
    if dataValidationUtil.validatePickListValueForTable(initRaceTable, initRecordsTable):
        print("Validation Passed")
        cur.execute(rf'''drop table if exists model.temp_records''')
        cur.execute(
            rf'''create table model.temp_records as select * from model.init_records where model.init_records.race_id not in (select distinct(race_id) from model.records)''')
        updateAfterDate = configUtil.latestUpdateDate()
        updateAfterDate = updateAfterDate.strftime("%Y-%m-%d")
        calculatedFieldUtil.updateNullData(updateAfterDate, 'model.temp_records', 'model.temp_records')
        calculatedFieldUtil.updateColumnType('model.temp_records')
        connection.commit()
        fromRecordsTable = 'model.records'
        toRecordsTable = 'model.records'
        fromRaceTable = 'model.race'
        toRaceTable = 'model.race'
        insertNewRowToModelTable(initRaceTable, 'model.temp_records', toRaceTable, toRecordsTable)
        dataPreprocessUtil.calculationBeforePredict(updateAfterDate, fromRaceTable, toRaceTable, fromRecordsTable,
                                                    toRecordsTable,
                                                    True)
        dataPreprocessUtil.calculationForPredict(updateAfterDate, 'model.records', 'model.records')
        calculatedFieldUtil.labelUpdate(updateAfterDate, toRecordsTable)
    else:
        raise Exception("Field Validation Failed")

def insertIntoPastRaceRecords(fromRaceTable, fromRecordsTable, toRaceTable, toRecordsTable , earliestDate):
    cur.execute(f'''drop table if exists {toRaceTable}''')
    cur.execute(
        rf'''create table {toRaceTable} as select * from {fromRaceTable} where race_date > %s;''', [earliestDate])
    cur.execute(f'''drop table if exists {toRecordsTable}''')
    cur.execute(
        rf'''create table {toRecordsTable} as select * from {fromRecordsTable} where records_date > %s;''', [earliestDate])
    connection.commit()


def main():
    scrapLatestRaceDate()
    patchingSectionalTimeUrl_2_2.main()
    scrapRaceData('init.all_race_day', 'init.all_race_url', 'init.init_race', 'init.init_records')
    initLoadToModelTable()
    versionList = configUtil.allReadyVersion()
    for version in versionList:
        predictDf = modelDataUtil.loadNewDataWithinAYearForPrediction('model.records', 'model.race', version)
        if len(predictDf) > 0:
            loadModelUtil.loadModelAndPredictDf(predictDf, 'model.records', f'dnnModel/{version}_ckpt', version)
    earliestDate = configUtil.earliestModelStartDate().pop()
    insertIntoPastRaceRecords('model.race', 'model.records', 'predict.past_race', 'predict.past_records', earliestDate)
    scrapHorseData_4.main()
    configUtil.updateLatestUpdateDate()