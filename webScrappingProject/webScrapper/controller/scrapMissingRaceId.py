from django.db import connection
from ..utils import scrapFromAllDayUrlUtil, webDriverUtil
from selenium import webdriver

recordsTable = 'init.init_records'
raceTable = 'init.init_race'
allRaceUrlTable = 'init.all_race_url'

cur = connection.cursor()

def main():
    options, chrome_driver_path = webDriverUtil.webDriverSetup()
    driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)

    cur.execute(rf'''select race_id, url from init.all_race_url where have_refunded is not true and no_data is not true and race_id not in (select distinct race_id from init.init_race)''')
    thisRaceUrlList = cur.fetchall()

    scrapFromAllDayUrlUtil.scrapAllThisRaceUrlFromAllRaceUrl(driver, thisRaceUrlList, raceTable, recordsTable,
                                                             allRaceUrlTable)
    driver.quit()
