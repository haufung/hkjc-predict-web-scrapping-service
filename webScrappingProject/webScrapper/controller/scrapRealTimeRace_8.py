import datetime
import re

from bs4 import BeautifulSoup
from django.db import connection
from selenium import webdriver

from ..utils import scrapRaceUtil, scrapHorseUtil
from ..utils import seleniumUtil, webDriverUtil

cur = connection.cursor()
initUrl = 'https://bet.hkjc.com/racing/pages/odds_wp.aspx?lang=en&dv=local'
initUrlForDeclareWeight = 'https://racing.hkjc.com/racing/information/English/racing/RaceCard.aspx'
race_url_button = "div[id^='raceSel']"
header_css = '#horseTable > thead > tr > td'
race_info_css = '#container > div > div > div.bodyMainOddsTable.content > div:nth-child(3) > div:nth-child(1) > span.content'
button_css = '#raceSel'
currentPredictTable = 'predict.current_records'
currentPredictRaceTable = 'predict.current_race'
toRaceTableName = 'model.race'
toUrlTableName = 'predict.current_race_url'
lang_switch_button_css = '#infoDiv > div.fMenuTABLE > div > div.fMenuRowLang > a'


def clearTableBeforeInsert():
    cur.execute(rf'''DELETE from {currentPredictTable};''')
    cur.execute(rf'''DELETE from {toUrlTableName};''')
    cur.execute(rf'''DELETE from {currentPredictRaceTable};''')
    cur.execute(rf'''ALTER TABLE {toRaceTableName} ADD COLUMN IF NOT EXISTS is_predict_value boolean;''')
    cur.execute(rf'''DELETE from {toRaceTableName} where is_predict_value = true;''')
    connection.commit()


def queryWebRecordsHeaderMapping():
    cur.execute(rf'''select * from predict.hkjc_web_records_mapping;''')
    return dict(cur.fetchall())


def queryRecordsRegexExtract():
    cur.execute(rf'''select * from config.records_regex_extract;''')
    return dict(cur.fetchall())


def checkIfJockeyTrainerTranslateExist(jockeyOrTrainer, name):
    cur.execute(rf'''select count(*) from config.{jockeyOrTrainer}_name_translate where code = %s;''', [name])
    return cur.fetchall()[0][0] == 1


def queryTranslateConfig(jockeyOrTrainer, name):
    cur.execute(rf'''select code,english,chinese from config.{jockeyOrTrainer}_name_translate where code = %s;''', [name])
    return cur.fetchall()[0]


def checkIfRaceNameTranslateExist(raceName):
    cur.execute(rf'''select count(*) from config.race_name_english_chinese_map where english_name = %s;''', [raceName])
    return cur.fetchall()[0][0] == 1


def insertIntoCurrentRecordsTable(tableName, raceId, recordsDate, columnNameList, listOfRowResult, horse_id):
    dbColumn = '"' + '","'.join(columnNameList) + '"'
    placeHolder = ['%s,'] * len(columnNameList)
    placeHolder = "".join(placeHolder)
    placeHolder = placeHolder[:len(placeHolder) - 1]
    cur.execute(rf'''INSERT INTO {tableName}
                ({dbColumn}, race_id, records_date, horse_id)
                VALUES
                ({"".join(placeHolder)},%s,%s,%s)''', listOfRowResult + [raceId, recordsDate, horse_id])


def insertIntoCurrentPredictUrlTable(urlTableName, raceId, url):
    cur.execute(rf'''INSERT INTO {urlTableName}
                ("race_id", "url")
                VALUES
                (%s,%s)''',
                [raceId, url])


def upsertJockeyTrainerNameTranslate(jockeyEnglishName, trainerEnglishName, jockeyChineseName, trainerChineseName):
    for index, jockeyName in enumerate(jockeyEnglishName):
        if jockeyName != 'SCR' and jockeyName != '':
            if checkIfJockeyTrainerTranslateExist('jockey', jockeyName):
                code, english, chinese = queryTranslateConfig('jockey', jockeyName)
                if chinese is None:
                    cur.execute(rf'''UPDATE config.jockey_name_translate set chinese = %s
                                     WHERE english = %s''', [jockeyChineseName[index], jockeyEnglishName[index]])
            else:
                cur.execute(rf'''INSERT INTO config.jockey_name_translate
                                    ("code", "english", "chinese")
                                    VALUES
                                    (%s,%s,%s)''', [jockeyEnglishName[index], jockeyEnglishName[index], jockeyChineseName[index]])
            connection.commit()
    for index2, trainerName in enumerate(trainerEnglishName):
        if trainerName != 'SCR' and trainerName != '':
            if checkIfJockeyTrainerTranslateExist('trainer', trainerName):
                code, english, chinese = queryTranslateConfig('trainer', trainerName)
                if chinese is None:
                    cur.execute(rf'''UPDATE config.trainer_name_translate set chinese = %s
                                         WHERE english = %s''', [trainerChineseName[index2], trainerEnglishName[index2]])
            else:
                cur.execute(rf'''INSERT INTO config.jockey_name_translate
                                        ("code", "english", "chinese")
                                        VALUES
                                        (%s,%s,%s)''', [trainerEnglishName[index2], trainerEnglishName[index2], trainerChineseName[index2]])
            connection.commit()


def insertRaceNameIfNotExist(english, chinese):
    if checkIfRaceNameTranslateExist(english) is False:
        cur.execute(rf'''INSERT INTO config.race_name_english_chinese_map
                                        ("english_name", "chinese_name")
                                        VALUES
                                        (%s,%s)''', [english, chinese])
        connection.commit()


def insertIntoRaceTableWithPredictFlag(tableName, raceId, raceDate, location, raceClass, distance, going, course):
    cur.execute(rf'''INSERT INTO {tableName}
                ("race_id", "race_date", "location", "class", "distance", "going", "course", "is_predict_value")
                VALUES
                (%s,%s,%s,%s,%s,%s,%s, true)''',
                [raceId, raceDate, location, raceClass, distance, going, course])


def insertIntoCurrentRaceTable(tableName, raceId, raceDate, raceTime, location, raceClass, distance,
                               going, course, raceNameEnglish, raceNo):
    cur.execute(rf'''INSERT INTO {tableName}
                ("race_id", "race_date","race_time", "location", "class", "distance", "going", "course", "race_name_english", "race_number")
                VALUES
                (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)''',
                [raceId, raceDate, raceTime, location, raceClass, distance, going, course, raceNameEnglish, raceNo])


def  scrapRecordsData(dri, url):
    dri.get(url)
    seleniumUtil.checkLoading(dri, race_info_css)
    soup = BeautifulSoup(dri.page_source, 'html.parser')
    noOfPage = len(soup.select(race_url_button))
    for raceNo in range(noOfPage):
        button = dri.find_element_by_css_selector(button_css + str(raceNo + 1))
        button.click()
        seleniumUtil.checkLoading(dri, race_info_css)
        soup = BeautifulSoup(dri.page_source, 'html.parser')
        raceInfo = soup.select(race_info_css)
        raceInfoList = raceInfo[0].text.split(", ")
        infoToCollect = [None, None, None, None, None, None, None, None]
        for i in range(len(raceInfoList)):
            infoToCollect[i] = raceInfoList[i]
        if "ALL WEATHER TRACK" in infoToCollect:
            raceName, raceDate, raceTime, raceClass, course, distance, going, _ = infoToCollect
        else:
            raceName, raceDate, raceTime, raceClass, course1, course2, distance, going = infoToCollect
            if course1 is not None and course2 is not None:
                course = " - ".join([course1, course2])
            else:
                course = None
        raceDate = datetime.datetime.strptime(raceDate, '%d/%m/%Y').date()
        raceTime = datetime.datetime.strptime(raceTime, '%H:%M').time()
        distance = distance.upper()
        if going is not None:
            going = going.upper()
        location = soup.select("#divMeetingInfo > div.mtgInfoLeft > div.mtgInfoDV > nobr:nth-child(2)")[0].text
        location = scrapRaceUtil.scrapLocation(location, dri.current_url)
        if raceNo + 1 < 10:
            raceNo = "0" + str(raceNo + 1)
        else:
            raceNo = str(raceNo + 1)
        raceId = raceDate.strftime('%Y%m%d') + "_" + raceNo

        recordsHeaderMapping = queryWebRecordsHeaderMapping()
        regexExtractMapping = queryRecordsRegexExtract()
        headerList = soup.select(header_css)
        numOfRow = len(soup.select('#horseTable > tbody > tr > td:not([class*="tableContentEnd"]):nth-child(1)'))
        dict(cur.fetchall())
        jockeyEnglishName = []
        trainerEnglishName = []
        for i in range(numOfRow):
            dbColumnNameList = []
            listOfRowResult = []
            rowTable = soup.select(f'#horseTable > tbody > tr:nth-child({i + 1}) > td')
            for index2, _ in enumerate(rowTable):
                thisHeader = headerList[index2]
                if thisHeader.text == 'Colour':
                    continue
                if thisHeader.text == 'Wt.' and rowTable[index2].text.isdigit():
                    exec(recordsHeaderMapping[thisHeader.text] + f' = int(rowTable[{index2}].text)')
                elif thisHeader.text == 'Wt.' and rowTable[index2].text.isdigit() is False:
                    exec(recordsHeaderMapping[thisHeader.text] + f' = None')
                elif thisHeader.text == 'Jockey' or thisHeader.text == 'Trainer':
                    exec(recordsHeaderMapping[thisHeader.text] + f''' = " ".join(re.findall(r"\w+|'|-", rowTable[{index2}].text))''')
                    if thisHeader.text == 'Jockey':
                        jockeyEnglishName.append(" ".join(re.findall(r"\w+|'|-", rowTable[index2].text)))
                    elif thisHeader.text == 'Trainer':
                        trainerEnglishName.append(" ".join(re.findall(r"\w+|'|-", rowTable[index2].text)))
                else:
                    if thisHeader.text == 'Horse Name':
                        horseId = re.findall(r"\('(.*?)'\)", rowTable[index2].select("a")[0]['href'])[0]
                    exec(recordsHeaderMapping[thisHeader.text] + f' = rowTable[{index2}].text')
                dbColumnNameList.append(recordsHeaderMapping[thisHeader.text])

                if thisHeader.text in regexExtractMapping:
                    exec(
                        f'{recordsHeaderMapping[thisHeader.text]} = re.findall(r"{regexExtractMapping[thisHeader.text]}", {recordsHeaderMapping[thisHeader.text]})[0]')

                listOfRowResult.append(eval(recordsHeaderMapping[thisHeader.text]))
            if len(listOfRowResult) > 0 and listOfRowResult[0].isdigit() and listOfRowResult[2] != '(SCR)':
                insertIntoCurrentRecordsTable(currentPredictTable, raceId, raceDate, dbColumnNameList, listOfRowResult,
                                              horseId)

        chineseLangButton = dri.find_element_by_css_selector(lang_switch_button_css)
        chineseLangButton.click()
        seleniumUtil.checkLoading(dri, race_info_css)
        soup = BeautifulSoup(dri.page_source, 'html.parser')
        raceInfoChi = soup.select(race_info_css)
        raceInfoChiList = raceInfoChi[0].text.split(", ")
        infoToCollectChi = [None, None, None, None, None, None, None, None]
        for i in range(len(raceInfoChiList)):
            infoToCollectChi[i] = raceInfoChiList[i]
            raceNameChi, _, _, _, _, _, _, _ = infoToCollectChi
        headerChineseList = soup.select(header_css)
        numOfRowChinese = len(soup.select('#horseTable > tbody > tr > td:not([class*="tableContentEnd"]):nth-child(1)'))
        jockeyChineseName = []
        trainerChineseName = []
        for i in range(numOfRowChinese):
            rowTable = soup.select(f'#horseTable > tbody > tr:nth-child({i + 1}) > td')
            for index2, _ in enumerate(rowTable):
                thisHeader = headerChineseList[index2]
                if thisHeader.text == '騎師':
                    jockeyChineseName.append(" ".join(re.findall(r"\w+|'|-", rowTable[index2].text)))
                elif thisHeader.text == '練馬師':
                    trainerChineseName.append(" ".join(re.findall(r"\w+|'|-", rowTable[index2].text)))

        upsertJockeyTrainerNameTranslate(jockeyEnglishName, trainerEnglishName, jockeyChineseName, trainerChineseName)
        insertRaceNameIfNotExist(raceName, raceNameChi)
        insertIntoRaceTableWithPredictFlag(toRaceTableName, raceId, raceDate, location, raceClass, distance, going,
                                           course)
        insertIntoCurrentRaceTable(currentPredictRaceTable, raceId, raceDate, raceTime, location, raceClass, distance,
                                   going, course, raceName, raceNo)
        insertIntoCurrentPredictUrlTable(toUrlTableName, raceId, dri.current_url)
        connection.commit()
        englishLangButton = dri.find_element_by_css_selector(lang_switch_button_css)
        englishLangButton.click()
        seleniumUtil.checkLoading(dri, race_info_css)


def queryMissingHorse(tableName):
    cur.execute(
        rf'''select horse_url, horse_name_english from {tableName} where horse_id not in (select horse_id from init.init_horse)''')
    return cur.fetchall()


def updateRecordsId(tableName):
    cur.execute(rf'''update {tableName} set records_id = alias.records_id from
(SELECT concat(race_id,'_',horse_id) as records_id, race_id, horse_id from {tableName}) as alias
where {tableName}.race_id = alias.race_id and {tableName}.horse_id = alias.horse_id;''')
    connection.commit()


def updateDeclareWeight(tableName, horse_name_english, declare_weight, horseUrl):
    cur.execute(rf'''UPDATE {tableName}
                set declare_weight = %s, horse_url = %s where horse_name_english = %s''',
                [declare_weight, horseUrl, horse_name_english])
    connection.commit()


def scrapDeclareWeightAndHorseUrl(dri, url, tableName):
    dri.get(url)
    raceButtonList = 'body > div > div.racingNum.top_races.js_racecard_rt_num > table > tbody > tr > td:not([class]) img'
    seleniumUtil.checkLoading(dri, raceButtonList)
    soup = BeautifulSoup(dri.page_source, 'html.parser')
    noOfPage = len(soup.select(raceButtonList))
    for num in range(noOfPage):
        noOfRowCss = f'#racecardlist > tbody > tr > td > table > tbody > tr > td:nth-child(4) > a'
        buttonCss = f'body > div > div.racingNum.top_races.js_racecard_rt_num > table > tbody > tr > td:nth-child({num + 2})'
        seleniumUtil.checkLoading(dri, buttonCss)
        button = dri.find_element_by_css_selector(buttonCss)
        button.click()
        seleniumUtil.checkLoading(dri, noOfRowCss)
        soup = BeautifulSoup(dri.page_source, 'html.parser')
        noOfRow = len(soup.select(noOfRowCss))
        for rowNo in range(noOfRow):
            horseName = soup.select(
                f'#racecardlist > tbody > tr > td > table > tbody > tr:nth-child({rowNo + 1}) > td:nth-child(4)')[
                0].text
            horseUrl = soup.select(
                f'#racecardlist > tbody > tr > td > table > tbody > tr:nth-child({rowNo + 1}) > td:nth-child(4) > a')[
                0]['href']
            declareWeight = soup.select(
                f'#racecardlist > tbody > tr > td > table > tbody > tr:nth-child({rowNo + 1}) > td:nth-child(14)')[
                0].text
            if declareWeight.isdigit():
                updateDeclareWeight(tableName, horseName, int(declareWeight), 'https://racing.hkjc.com' + horseUrl)
            else:
                updateDeclareWeight(tableName, horseName, None, 'https://racing.hkjc.com' + horseUrl)

def main():
    options, chrome_driver_path = webDriverUtil.webDriverSetup()
    driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)

    clearTableBeforeInsert()
    scrapRecordsData(driver, initUrl)
    updateRecordsId(currentPredictTable)
    scrapDeclareWeightAndHorseUrl(driver, initUrlForDeclareWeight, currentPredictTable)

    missingHorse = queryMissingHorse(currentPredictTable)
    if len(missingHorse) > 0:
        for missHorseTuple in missingHorse:
            print(f"Scrapping new horse... '{missHorseTuple[1]}' {missHorseTuple[0]}")
            scrapHorseUtil.scrapHorseData(driver, missHorseTuple[0], "init.init_horse", None, missHorseTuple[1])
    updateRecordsId(currentPredictTable)
    driver.quit()
