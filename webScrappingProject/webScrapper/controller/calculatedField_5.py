from django.db import connection
from ..utils import calculatedFieldUtil, dataPreprocessUtil

initRecordsTable = 'model.init_records'
initRaceTable = 'model.init_race'
fromRecordsTable = 'model.records'
toRecordsTable = 'model.records'
fromRaceTable = 'model.race'
toRaceTable = 'model.race'
cur = connection.cursor()


def buildTable(initRaceTable, initRecordsTable, toRaceTable, toRecordsTable):
    cur.execute(rf'''DROP TABLE IF EXISTS {toRaceTable};''')
    cur.execute(rf'''DROP TABLE IF EXISTS {toRecordsTable};''')
    cur.execute(rf'''create table {toRaceTable} as select * from {initRaceTable}''')
    cur.execute(rf'''create table {toRecordsTable} as select * from {initRecordsTable}''')
    connection.commit()
    print("build table done!")

def main():
    # uncomment when build table again
    # buildTable(initRaceTable, initRecordsTable, toRaceTable, toRecordsTable)
    # calculatedFieldUtil.buildIndex(toRaceTable, toRecordsTable)
    # calculatedFieldUtil.updateNullData('1990-01-01', toRecordsTable, toRecordsTable)
    # calculatedFieldUtil.updateColumnType(toRecordsTable)

    dataPreprocessUtil.calculationBeforePredict('1990-01-01', fromRaceTable, toRaceTable, fromRecordsTable, toRecordsTable,
                                                True)

    # calculatedFieldUtil.labelUpdate('1990-01-01', toRecordsTable)
