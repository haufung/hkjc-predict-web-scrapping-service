from django.db import connection
from selenium import webdriver

from ..utils import scrapRaceUtil, webDriverUtil
from ..utils.scrapRaceUtil import RACE_NAME_CSS_SELECTOR_1

cur = connection.cursor()


def queryDistinctRaceEnglishName():
    cur.execute(f'''select * from config.race_name_english_chinese_map where chinese_name is null''')
    return cur.fetchall()


def queryUrlByRaceName(engName):
    cur.execute(
        f'''select url from (select race_id from init.init_race where race_name_english = %s order by race_date desc limit 1) as alias, init.all_race_url
where alias.race_id = init.all_race_url.race_id;''', [engName])
    return cur.fetchall()


def updateRaceChiNameByEngName(engName, chiName):
    cur.execute(f'''update config.race_name_english_chinese_map set chinese_name = %s where english_name = %s;''',
                [chiName, engName])
    connection.commit()

def insertNewChineseRaceName():
    cur.execute(f'''insert into config.race_name_english_chinese_map select distinct(race_name_english) as english_name from model.race where race_name_english not in (select english_name from config.race_name_english_chinese_map);''')
    connection.commit()


def main():
    insertNewChineseRaceName()
    allRaceEng = queryDistinctRaceEnglishName()
    if len(allRaceEng) > 0:
        options, chrome_driver_path = webDriverUtil.webDriverSetup()
        driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
        for raceNameTuple in allRaceEng:
            raceEngName = raceNameTuple[0]
            urlTuple = queryUrlByRaceName(raceEngName)
            url = urlTuple[0][0].replace("English", "Chinese")
            driver.get(url)
            soup = scrapRaceUtil.checkingAfterEnteringUrl(url, driver, raceEngName, "PATCH RACE CHINESE NAME")
            if soup is False:
                return
            raceChiName = soup.select(RACE_NAME_CSS_SELECTOR_1)[0].text
            updateRaceChiNameByEngName(raceEngName, raceChiName)
        driver.quit()
