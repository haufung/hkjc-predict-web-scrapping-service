from django.db import connection
from selenium import webdriver

from ..utils import webDriverUtil, scrapHorseUtil

cur = connection.cursor()


def queryAllHorseId():
    cur.execute(rf'''select * from init.all_horse_url where horse_id not in (select horse_id from init.init_horse)''')
    return cur.fetchall()


def main():
    options, chrome_driver_path = webDriverUtil.webDriverSetup()
    driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
    scrapHorseUtil.generateHorseLink(driver, scrapHorseUtil.queryDistinctHorseIdFromRecords())
    horseUrlTupleList = queryAllHorseId()
    for urlTuple in horseUrlTupleList:
        scrapHorseUtil.scrapHorseData(driver, urlTuple[1], "init.init_horse", urlTuple[0], None)
    driver.quit()
