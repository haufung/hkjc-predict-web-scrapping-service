from django.db import connection
from selenium import webdriver
from ..utils import scrapRaceUtil
from ..utils.raceUrlUtil import generateRaceIdFromUrl

from ..utils import seleniumUtil, webDriverUtil
from ..utils.debugLogUtil import removeFromDebugLogByUrl


cur = connection.cursor()


recordsTable = 'init.init_records'
raceTable = 'init.init_race'
allRaceUrlTable = 'init.all_race_url'


def checkIfThisRaceIdExist(id):
    cur.execute(rf'''select * from init.init_race where race_id = %s;''', [id])
    return len(cur.fetchall()) > 0


def checkIfFirstRaceIdExist(idLike):
    cur.execute(rf'''select * from init.init_race where race_id like %s;''', [idLike])
    return len(cur.fetchall()) > 0


def scrapAllThisRaceUrl(thisRaceUrlList, forceNoRedund):
    for raceTuple in thisRaceUrlList:
        url = raceTuple[0]
        driver.get(url)
        seleniumUtil.checkLoading(driver)
        driverUrl = driver.current_url
        raceId, _ = generateRaceIdFromUrl(driverUrl)
        if raceId is not None and checkIfThisRaceIdExist(raceId) is False:
            headerListMapping = scrapRaceUtil.queryInitRecordsHeaderMapping()
            scrapRaceUtil.scrapThisRaceUrl(driver, headerListMapping, driverUrl, None, None, None,
                                           forceNoRedund, raceTable, recordsTable, allRaceUrlTable)
            if checkIfThisRaceIdExist(raceId) is True:
                print(f"race_id scraped successfully : {driverUrl} ... removing from debug_log")
                removeFromDebugLogByUrl(url)
        else:
            print(f"race_id exist already : {driverUrl} ... removing from debug_log")
            removeFromDebugLogByUrl(url)


def scrapAllFirstRaceUrl(urlList):
    for raceTuple in urlList:
        url = raceTuple[0]
        driver.get(url)
        seleniumUtil.checkLoading(driver)
        driverUrl = driver.current_url
        _, raceId = generateRaceIdFromUrl(driverUrl)
        raceIdlike = raceId.split("_")[0] + "%"
        if raceId is not None and checkIfFirstRaceIdExist(raceIdlike) is False:
            headerListMapping = scrapRaceUtil.queryInitRecordsHeaderMapping()
            scrapRaceUtil.scrapFirstRaceUrl(driver, headerListMapping, None, driverUrl, True)
            if checkIfFirstRaceIdExist(raceIdlike) is True:
                print(f"race_id scraped successfully : {driverUrl} ... removing from debug_log")
                removeFromDebugLogByUrl(url)
        else:
            print(f"race_id exist already : {driverUrl} ... removing from debug_log")
            removeFromDebugLogByUrl(url)


def identifyRefundedUrlInAllRaceUrlTable(dri, urlList):
    for urlTuple in urlList:
        url = urlTuple[0]
        headerListMapping = scrapRaceUtil.queryInitRecordsHeaderMapping()
        scrapRaceUtil.scrapFirstRaceUrl(dri, headerListMapping, None, url, None, True, raceTable, recordsTable, allRaceUrlTable)

def main():
    options, chrome_driver_path = webDriverUtil.webDriverSetup()
    driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
    # cur.execute(rf'''select url from init.debug_log where general_reference = 'this race url';''')
    # scrapAllThisRaceUrl(cur.fetchall(), None)
    # cur.execute(rf'''select url from init.debug_log where manual_force_no_refund is true;''')
    # scrapAllThisRaceUrl(cur.fetchall(), True)
    # cur.execute(rf'''select url from init.debug_log where general_reference = 'first_race_url';''')
    # scrapAllFirstRaceUrl(cur.fetchall())

    cur.execute(
        rf'''select url from init.all_race_day, ( select race_date, alias2.count from (select max(race_number::integer), race_date from init.init_race group by race_date) as alias1 ,(select count(*), alias.records_date  from (select count(*), race_id, records_date from init.init_records group by race_id,records_date) as alias group by alias.records_date) as alias2
    where alias1.race_date = alias2.records_date and alias1.max != alias2.count) as alias3 where alias3.race_date = init.all_race_day.race_date''')
    identifyRefundedUrlInAllRaceUrlTable(driver, cur.fetchall())

    driver.quit()
