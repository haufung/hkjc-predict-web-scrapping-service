from datetime import datetime

import numpy as np
import pandas as pd
import tensorflow as tf
from django.db import connection
from tensorflow import keras

from ..utils import modelDataUtil, loadModelUtil, modelRequiredHeaderUtil, configUtil

cur = connection.cursor()


def demoFeatureColumn(thisFeatureColumn, exampleBatch):
    feature_layer = tf.keras.layers.DenseFeatures(thisFeatureColumn)
    print(feature_layer(exampleBatch).numpy())


def evalImbalanceOfDataSet(df):
    neg, pos = pd.np.bincount(df['label'])
    total = neg + pos
    print('Examples:\n    Total: {}\n    Positive: {} ({:.2f}% of total)\n'.format(
        total, pos, 100 * pos / total))
    return neg, pos


def trainModel(version):
    trainDf = modelDataUtil.loadTrainingSetOrderByRecordsDateBeforeYear('model.records_with_balanced_label',
                                                                        'model.race', 2, version)
    valDf = modelDataUtil.loadValidationSetOrderByRecordsDateAfterYear('model.records_with_balanced_label',
                                                                       'model.race', 2, version)
    testDf = modelDataUtil.loadTestingSetOrderByRecordsDate('model.records_with_balanced_label', 'model.race', version)

    neg, pos = evalImbalanceOfDataSet(trainDf)

    # trainDf, valDf = train_test_split(totalDf, test_size=0.2)
    # print(f"totalDf = {totalDf.size}")
    print(f"trainDf = {trainDf.size}")
    print(f"valDf = {valDf.size}")
    batch_size = 2000
    train_ds, _ = loadModelUtil.df_to_dataset(trainDf, batch_size)
    val_ds, _ = loadModelUtil.df_to_dataset(valDf, batch_size, shuffle=False)
    test_ds, _ = loadModelUtil.df_to_dataset(testDf, batch_size, shuffle=False)

    # example_batch = next(iter(train_ds))[0]
    # for featureColumn in featureColumnList:
    #     demoFeatureColumn(featureColumn, example_batch)

    # weighted_model.load_weights(initial_weights)

    early_stopping = tf.keras.callbacks.EarlyStopping(
        monitor='val_loss',
        verbose=1,
        patience=100,
        mode='min',
        restore_best_weights=True)

    logdir = "dnnModel/logs/scalars/" + datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)

    initial_bias = np.log([pos / neg])

    featureColumnList = loadModelUtil.getFeatureColumn(version)
    model = loadModelUtil.makeModel(featureColumnList, output_bias=initial_bias)

    model.compile(optimizer=keras.optimizers.Adam(lr=0.0001),
                  loss=keras.losses.BinaryCrossentropy(),
                  metrics=[
                      keras.metrics.BinaryAccuracy(name='accuracy'),
                      keras.metrics.Precision(name='precision'),
                      keras.metrics.Recall(name='recall'),
                      keras.metrics.AUC(name='auc'),
                      keras.metrics.TruePositives(name='tp'),
                      keras.metrics.FalsePositives(name='fp'),
                      keras.metrics.TrueNegatives(name='tn'),
                      keras.metrics.FalseNegatives(name='fn'),
                  ])

    results = model.evaluate(train_ds)
    print("Loss: {:0.4f}".format(results[0]))

    # weight_for_0 = (1 / neg) * (neg + pos) / 2.0
    # weight_for_1 = (1 / pos) * (neg + pos) / 2.0
    weight_for_0 = 1
    weight_for_1 = 1

    class_weight = {0: weight_for_0, 1: weight_for_1}
    weighted_history = model.fit(train_ds,
                                 validation_data=val_ds,
                                 epochs=100000,
                                 callbacks=[early_stopping, tensorboard_callback],
                                 class_weight=class_weight,
                                 use_multiprocessing=True)

    model.save_weights(f"dnnModel/{version}_ckpt")
    results = model.evaluate(test_ds)


def addNewColumnToBalancedLabelTable(version):
    cur.execute(rf'''SELECT COLUMN_NAME 
        FROM INFORMATION_SCHEMA.COLUMNS 
        WHERE TABLE_SCHEMA='model' 
            AND TABLE_NAME='records';''')
    allRecordsColumn = [r[0] for r in cur.fetchall()]

    cur.execute(rf'''SELECT COLUMN_NAME 
            FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE TABLE_SCHEMA='model' 
                AND TABLE_NAME='records_with_balanced_label';''')
    allBalancedRecordsColumn = [r[0] for r in cur.fetchall()]

    newColumn = [x for x in allRecordsColumn if x not in allBalancedRecordsColumn]
    neededColumn = modelRequiredHeaderUtil.recordsHeader(version)

    columnToAdd = [x for x in newColumn if x in neededColumn]

    for column in columnToAdd:
        cur.execute(rf'''SELECT data_type 
                FROM INFORMATION_SCHEMA.COLUMNS 
                WHERE TABLE_SCHEMA='model' 
                    AND TABLE_NAME='records' and column_name = %s;''', [column])
        dataType = [r[0] for r in cur.fetchall()].pop()
        print(f"Adding {column} {dataType} ...")
        cur.execute(rf'''ALTER TABLE model.records_with_balanced_label ADD COLUMN IF NOT EXISTS {column} {dataType};''')
        cur.execute(
            rf'''Update model.records_with_balanced_label set {column} = model.records.{column} from model.records where model.records.records_id = model.records_with_balanced_label.records_id;''')
        connection.commit()


def declareAccuracyWithThresholdFunction(version):
    cur.execute(
        rf'''CREATE OR REPLACE FUNCTION calculate_accuracy_with_threshold(threshold float, model_start_date date)
    RETURNS float AS $$
    declare
        threshold_accuracy float;
        this_all_win_count integer;
    BEGIN 
    select count(*) as all_wins from model.records 
    where prediction_{version} > threshold 
    and predicted_win_{version} = true 
    and records_date > model_start_date 
    into this_all_win_count;
    
    if this_all_win_count = 0 then
            RETURN 0;
    else
    select alias.correct_wins::float/alias2.all_wins::float as accuracy from
    (select count(*) as correct_wins from model.records where prediction_{version} > threshold and (place = '1' or place = '1 DH') and predicted_win_{version} = true and records_date > model_start_date) as alias,
    (select count(*) as all_wins from model.records where prediction_{version} > threshold and predicted_win_{version} = true and records_date > model_start_date) as alias2
    into threshold_accuracy;
	RETURN threshold_accuracy;
    end if;
    END;
    $$ LANGUAGE plpgsql;''')


def declareAvgWinOddsWithThreshold(version):
    cur.execute(rf'''CREATE OR REPLACE FUNCTION average_win_odds_with_threshold(threshold float, model_start_date date)
    RETURNS float AS $$
    declare
        this_average_win_odds float;
    BEGIN 
    select avg(win_odds) as average_win_odds from model.records 
    where prediction_{version} > threshold and predicted_win_{version} = true and (place = '1' or place = '1 DH') and records_date > model_start_date
    into this_average_win_odds;
	RETURN this_average_win_odds;
    END;
    $$ LANGUAGE plpgsql;''')


def declareWinOrLoseBetOnThreshold(version, winOrLose):
    if winOrLose == 'win':
        query = '''(place = '1' or place = '1 DH')'''
    else:
        query = '''(place != '1' and place != '1 DH')'''
    cur.execute(rf'''CREATE OR REPLACE FUNCTION {winOrLose}_bet_on_threshold(threshold float, model_start_date date)
    RETURNS integer AS $$
    declare
        bet_count integer;
    BEGIN 
    select count(*) as bet_count from model.records where prediction_{version} > threshold and {query} and predicted_win_{version} = true and records_date > model_start_date into bet_count;
	RETURN bet_count;
    END;
    $$ LANGUAGE plpgsql;''')


def removePreviousVersionRecords(version):
    cur.execute(f'DELETE FROM predict.threshold_performance where version = %s;', [version])
    connection.commit()


def insertToThresholdTable(version, avgWinOdds, threshold, accuracy, expectedProfit, winBetCounts, loseBetCounts):
    cur.execute(f'''INSERT INTO predict.threshold_performance
            ("version",
            "avg_win_odds",
            "threshold",
            "accuracy",
            "expected_profit",
            "win_bet_count",
            "lose_bet_count")
            VALUES
            (%s, %s, %s, %s, %s, %s, %s)''', [version, avgWinOdds, threshold, accuracy, expectedProfit, winBetCounts, loseBetCounts])
    connection.commit()


def calculateThresholdPerformance(version):
    print("calculating threshold performance...")
    versionString = version.replace(".", "_")
    modelStartDate = configUtil.modelStartDate(version)[0]
    declareAccuracyWithThresholdFunction(versionString)
    declareAvgWinOddsWithThreshold(versionString)
    declareWinOrLoseBetOnThreshold(versionString, 'win')
    declareWinOrLoseBetOnThreshold(versionString, 'lose')
    removePreviousVersionRecords(version)
    threshold = 0.50
    while threshold < 1:
        cur.execute(f'select lose_bet_on_threshold({threshold}, %s)', [modelStartDate])
        loseBetCounts = cur.fetchall()[0][0]
        if loseBetCounts == 0:
            break

        cur.execute(f'select win_bet_on_threshold({threshold}, %s)', [modelStartDate])
        winBetCounts = cur.fetchall()[0][0]
        cur.execute(f'select calculate_accuracy_with_threshold({threshold}, %s)', [modelStartDate])
        accuracy = cur.fetchall()[0][0]
        cur.execute(f'select average_win_odds_with_threshold({threshold}, %s)', [modelStartDate])
        avgWinOdds = cur.fetchall()[0][0]

        insertToThresholdTable(version, round(avgWinOdds, 2), threshold, round(accuracy, 2), round(avgWinOdds * accuracy, 2), winBetCounts, loseBetCounts)
        threshold = threshold + 0.01
        threshold = round(threshold, 2)
    return


def startTrainModel(version):
    # addNewColumnToBalancedLabelTable(version)
    # trainModel(version)
    # dataPreprocessUtil.calculationForPredict('2019-10-06', 'model.records', 'model.records')
    # modelConfigUtil.updateModelStartDate(version)
    predictDf = modelDataUtil.loadTestingSetOrderByRecordsDateForPrediction('model.records', 'model.race', version)
    loadModelUtil.loadModelAndPredictDf(predictDf, 'model.records', f'dnnModel/{version}_ckpt', version,
                                        clearAllField=True)
    calculateThresholdPerformance(version)
