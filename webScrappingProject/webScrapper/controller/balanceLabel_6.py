from django.db import connection

cur = connection.cursor()


def main():
    toRecordsTable = 'model.records'
    schemaAndName = toRecordsTable.split(".")
    schema = schemaAndName[0]
    tableName = schemaAndName[1]
    cur.execute(rf'''SELECT COLUMN_NAME 
    FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE TABLE_SCHEMA='{schema}' 
        AND TABLE_NAME='{tableName}';''')
    allColumn = [r[0] for r in cur.fetchall()]

    cur.execute(rf'''DROP TABLE IF EXISTS {schema}.records_with_balanced_label;''')
    connection.commit()
    cur.execute(rf'''CREATE TABLE {schema}.records_with_balanced_label AS 
                    select * from {toRecordsTable};''')
    connection.commit()

    cur.execute(rf'''CREATE OR REPLACE FUNCTION balance_label_for_this_race_id(race_id_input text) RETURNS void AS $$
    BEGIN
       INSERT INTO {schema}.records_with_balanced_label({",".join(allColumn)})
     select * from {schema}.records_with_balanced_label where race_id = race_id_input and label = '1' limit 1;
        END;
    $$ LANGUAGE plpgsql;''')

    cur.execute(
        rf'''create table IF NOT EXISTS {schema}.records_with_balanced_label as select * from {toRecordsTable};''')
    connection.commit()
    cur.execute(
        rf'''select race_id, count(*) as total_records from {toRecordsTable} where label is not null group by race_id order by race_id desc''')
    row = cur.fetchall()
    len(row)
    count = 0
    for (raceId, horseNum) in row:
        loopTime = int(horseNum) - 2
        for i in range(loopTime):
            cur.execute(rf'''select "balance_label_for_this_race_id"('{raceId}')''')
        connection.commit()
        count = count + 1
        print(rf"Complete {raceId}, {len(row) - count} remains")
