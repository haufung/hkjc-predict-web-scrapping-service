from django.db import connection

from ..utils import configUtil

cur = connection.cursor()


def declareCalculateProfitOfThisRace(versionString, withThreshold, betValue):
    if withThreshold:
        fieldName = "balance_threshold"
    else:
        fieldName = "balance"
    cur.execute(rf'''CREATE OR REPLACE FUNCTION compute_current_balance(this_race_id text, this_win_odds float, this_place text, threshold float)
        RETURNS float AS $$
        declare
          last_balance float;
          this_balance float;
        BEGIN
		
        select CASE
				WHEN count(*) = 0 THEN 0 
                ELSE (select {fieldName} from predict.model_{versionString} where race_id < this_race_id and prediction_{versionString} > threshold order by race_id desc limit 1 ) END AS balance
				from predict.model_{versionString} where race_id < this_race_id and prediction_{versionString} > threshold limit 1 into last_balance;
		
        select case 
                when (this_place = '1' or this_place = '1 DH') then (last_balance - {betValue} + this_win_odds * {betValue})
                else last_balance - {betValue} end as this_balance
        
          into this_balance;
           RETURN this_balance;
        END;
        $$ LANGUAGE plpgsql;''')


def startCalculate(version):
    threshold = configUtil.modelThreshold(version)[0]
    modelStartDate = configUtil.modelStartDate(version)[0]
    versionString = version.replace(".", "_")
    cur.execute(rf'''DROP TABLE IF EXISTS predict.model_{versionString};''')
    cur.execute(
        f'create table predict.model_{versionString} as select records_id, race_id, win_odds, place, prediction_{versionString}, null::float as balance, null::float as balance_threshold from model.records where predicted_win_{versionString} = true and records_date >= %s',
        [modelStartDate])

    cur.execute(f'ALTER TABLE predict.model_{versionString} ADD COLUMN IF NOT EXISTS balance float;')
    cur.execute(f'select race_id from predict.model_{versionString} order by race_id asc;')
    raceIdList = [r[0] for r in cur.fetchall()]
    betValue = 1
    declareCalculateProfitOfThisRace(versionString, False, betValue)
    for index, raceId in enumerate(raceIdList):
        cur.execute(f'update predict.model_{versionString} set balance = alias.this_balance from '
                    f'(select ROUND("compute_current_balance"(race_id, win_odds, place, 0)::numeric, 2) as this_balance, records_id from predict.model_{versionString} where race_id = %s) as alias '
                    f'where predict.model_{versionString}.records_id = alias.records_id;', [raceId])
        connection.commit()

    cur.execute(f'ALTER TABLE predict.model_{versionString} ADD COLUMN IF NOT EXISTS balance_threshold float;')
    cur.execute(f'select race_id from predict.model_{versionString} where prediction_{versionString} > %s order by race_id asc;', [threshold])
    raceIdList = [r[0] for r in cur.fetchall()]
    betValueForThreshold = 10
    declareCalculateProfitOfThisRace(versionString, True, betValueForThreshold)
    for index, raceId in enumerate(raceIdList):
        cur.execute(f'update predict.model_{versionString} set balance_threshold = alias.this_balance_threshold from '
                    f'(select ROUND("compute_current_balance"(race_id, win_odds, place, %s)::numeric, 2) as this_balance_threshold, records_id from predict.model_{versionString} where race_id = %s) as alias '
                    f'where predict.model_{versionString}.records_id = alias.records_id;', [threshold, raceId])
        connection.commit()
