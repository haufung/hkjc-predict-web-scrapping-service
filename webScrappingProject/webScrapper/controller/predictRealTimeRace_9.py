
from django.db import connection
from ..utils import calculatedFieldUtil, modelDataUtil, loadModelUtil, \
    dataPreprocessUtil, dataValidationUtil, configUtil

fromRecordsTable = 'model.records'
toRecordsTable = 'predict.current_records'
fromRaceTable = 'model.race'
toRaceTable = 'predict.current_race'

cur = connection.cursor()

def main():
    cur.execute(rf'''update {toRecordsTable} set place = '1';''')
    connection.commit()
    if dataValidationUtil.validatePickListValueForTable(toRaceTable, toRecordsTable):
        calculatedFieldUtil.updateNullData('1990-01-01', fromRecordsTable, toRecordsTable)
        dataPreprocessUtil.calculationBeforePredict('1990-01-01', fromRaceTable, toRaceTable, fromRecordsTable, toRecordsTable,
                                                    False)
        dataPreprocessUtil.calculationForPredict('1990-01-01', fromRecordsTable, toRecordsTable)
        cur.execute(f'''update {toRecordsTable} set label = 1;''')
        connection.commit()
        versionList = configUtil.allReadyVersion()
        for version in versionList:
            predictDf = modelDataUtil.loadTestingSetOrderByRecordsDateForPrediction(toRecordsTable, toRaceTable, version)
            loadModelUtil.loadModelAndPredictDf(predictDf, toRecordsTable, f'dnnModel/{version}_ckpt', version)
