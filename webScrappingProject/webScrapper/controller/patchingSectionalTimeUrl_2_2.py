import re

from bs4 import BeautifulSoup
from django.db import connection
from selenium import webdriver

from ..utils import sectionTimeUrlUtil, webDriverUtil, seleniumUtil

cur = connection.cursor()

# race_sectional_time_and_position
RACE_TIME_CSS_SELECTOR = ".Race > table > tbody > tr:nth-child(1) > td"
RACE_SEC_CSS_SELECTOR = ".Race > table > tbody > tr:nth-child(2) > td"
RECORDS_ROW_CSS_SELECTOR = ".race_table > tbody > tr"


def queryAllRaceDayUrlForSectionalTimeUrl():
    cur.execute(f'''select * from init.all_race_url where sectional_time_url is not null and race_id not in (select race_id from init.init_race_sectional_time_and_position) order by race_id desc;''')
    return cur.fetchall()


def updateDatabaseForSectionalTimeLink(raceId, sectionalTimeUrl):
    cur.execute(
        f'''update init.all_race_url set sectional_time_url = %s where race_id = %s and have_refunded is null and no_data is null;''',
        [sectionalTimeUrl, raceId])
    connection.commit()


def insertSectionalTimeAndPositionRace(raceId, raceTime1, raceTime2, raceTime3, raceTime4, raceTime5, raceTime6, raceFinTime,
                                       raceSec1, raceSec2, raceSec3, raceSec4, raceSec5, raceSec6):
    cur.execute(
        f'''INSERT INTO init.init_race_sectional_time_and_position 
        ("race_id",
        "race_time_1",
        "race_time_2",
        "race_time_3",
        "race_time_4",
        "race_time_5",
        "race_time_6",
        "race_fin_time",
        "race_sec_1",
        "race_sec_2",
        "race_sec_3",
        "race_sec_4",
        "race_sec_5",
        "race_sec_6") 
        VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);''',
        [raceId, raceTime1, raceTime2, raceTime3, raceTime4, raceTime5, raceTime6, raceFinTime, raceSec1, raceSec2, raceSec3,
         raceSec4, raceSec5, raceSec6])
    connection.commit()


def insertSectionalTimeAndPositionRecords(raceId, finishingOrder, horseNum, horseName, horseId, pos1, bl1, pos2, bl2,
                                          pos3, bl3, pos4, bl4, pos5, bl5, pos6, bl6, horseSec1, horseSec2, horseSec3,
                                          horseSec4, horseSec5, horseSec6, horseFinTime):
    cur.execute(
        f'''INSERT INTO init.init_records_sectional_time_and_position 
        ("race_id",
        "finishing_order",
        "horse_num",
        "horse_name",
        "horse_id",
        "position_1",
        "bl_1",
        "position_2",
        "bl_2",
        "position_3",
        "bl_3",
        "position_4",
        "bl_4",
        "position_5",
        "bl_5",
        "position_6",
        "bl_6",
        "horse_sec_1",
        "horse_sec_2",
        "horse_sec_3",
        "horse_sec_4",
        "horse_sec_5",
        "horse_sec_6",
        "horse_finishing_time") 
        VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);''',
        [raceId, finishingOrder, horseNum, horseName, horseId, pos1, bl1, pos2, bl2, pos3, bl3, pos4, bl4, pos5, bl5,
         pos6, bl6, horseSec1, horseSec2, horseSec3, horseSec4, horseSec5, horseSec6, horseFinTime])
    connection.commit()


def scrapSectionalTimeAndPositionRace(raceId, soup):
    raceTimeArr = soup.select(RACE_TIME_CSS_SELECTOR)
    raceTimeArr.pop(0)
    raceTime1, raceTime2, raceTime3, raceTime4, raceTime5, raceTime6, raceFinTime = [None] * 7
    if len(raceTimeArr) > 0:
        raceTime1 = raceTimeArr[0].text.replace("(", "").replace(")", "")
    if len(raceTimeArr) > 1:
        raceTime2 = raceTimeArr[1].text.replace("(", "").replace(")", "")
    if len(raceTimeArr) > 2:
        raceTime3 = raceTimeArr[2].text.replace("(", "").replace(")", "")
    if len(raceTimeArr) > 3:
        raceTime4 = raceTimeArr[3].text.replace("(", "").replace(")", "")
    if len(raceTimeArr) > 4:
        raceTime5 = raceTimeArr[4].text.replace("(", "").replace(")", "")
    if len(raceTimeArr) > 5:
        raceTime5 = raceTimeArr[5].text.replace("(", "").replace(")", "")

    raceFinTime = raceTimeArr[-1].text.replace("(", "").replace(")", "")

    raceSecArr = soup.select(RACE_SEC_CSS_SELECTOR)
    raceSecArr.pop(0)
    raceSec1, raceSec2, raceSec3, raceSec4, raceSec5, raceSec6 = None, None, None, None, None, None
    if len(raceTimeArr) > 0:
        raceSec1 = raceSecArr[0].text
        raceSec1 = re.search("\d+\.\d*", raceSec1).group()
    if len(raceTimeArr) > 1:
        raceSec2 = raceSecArr[1].text
        raceSec2 = re.search("\d+\.\d*", raceSec2).group()
    if len(raceTimeArr) > 2:
        raceSec3 = raceSecArr[2].text
        raceSec3 = re.search("\d+\.\d*", raceSec3).group()
    if len(raceTimeArr) > 3:
        raceSec4 = raceSecArr[3].text
        raceSec4 = re.search("\d+\.\d*", raceSec4).group()
    if len(raceTimeArr) > 4:
        raceSec5 = raceSecArr[4].text
        raceSec5 = re.search("\d+\.\d*", raceSec5).group()
    if len(raceTimeArr) > 5:
        raceSec6 = raceSecArr[5].text
        raceSec6 = re.search("\d+\.\d*", raceSec6).group()
    insertSectionalTimeAndPositionRace(raceId, raceTime1, raceTime2, raceTime3, raceTime4, raceTime5, raceTime6, raceFinTime,
                                       raceSec1, raceSec2, raceSec3, raceSec4, raceSec5, raceSec6)


def scrapSectionalTimeAndPositionRecords(raceId, soup):
    recordRowArr = soup.select(RECORDS_ROW_CSS_SELECTOR)
    for i in range(len(recordRowArr)):
        finishingOrderCss = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(1)"
        finishingOrder = soup.select(finishingOrderCss)[0].text.replace(" ", "")
        horseNumCss = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(2)"
        horseNum = soup.select(horseNumCss)[0].text
        horseNameAndIdCss = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(3)"
        horseNameAndId = soup.select(horseNameAndIdCss)[0].text

        horseName, horseId = horseNameAndId.split("(")[0], horseNameAndId.split("(")[1].replace(")", "")

        pos1, pos2, pos3, pos4, pos5, pos6, bl1, bl2, bl3, bl4, bl5, bl6 = [None] * 12
        pos1AndBl1Css = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(4)"
        pos2AndBl2Css = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(5)"
        pos3AndBl3Css = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(6)"
        pos4AndBl4Css = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(7)"
        pos5AndBl5Css = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(8)"
        pos6AndBl6Css = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(9)"

        checkIfPos1AndBl1Exist = len(soup.select(pos1AndBl1Css + " > img")) == 0
        checkIfPos2AndBl2Exist = len(soup.select(pos2AndBl2Css + " > img")) == 0
        checkIfPos3AndBl3Exist = len(soup.select(pos3AndBl3Css + " > img")) == 0
        checkIfPos4AndBl4Exist = len(soup.select(pos4AndBl4Css + " > img")) == 0
        checkIfPos5AndBl5Exist = len(soup.select(pos5AndBl5Css + " > img")) == 0
        checkIfPos6AndBl6Exist = len(soup.select(pos6AndBl6Css + " > img")) == 0

        if checkIfPos1AndBl1Exist:
            pos1AndBl1Soup = soup.select(pos1AndBl1Css)
            pos1 = pos1AndBl1Soup[0].select("p > span")[0].text
            bl1 = pos1AndBl1Soup[0].select("p > i")[0].text
        if checkIfPos2AndBl2Exist:
            pos2AndBl2Soup = soup.select(pos2AndBl2Css)
            pos2 = pos2AndBl2Soup[0].select("p > span")[0].text
            bl2 = pos2AndBl2Soup[0].select("p > i")[0].text
        if checkIfPos3AndBl3Exist:
            pos3AndBl3Soup = soup.select(pos3AndBl3Css)
            pos3 = pos3AndBl3Soup[0].select("p > span")[0].text
            bl3 = pos3AndBl3Soup[0].select("p > i")[0].text
        if checkIfPos4AndBl4Exist:
            pos4AndBl4Soup = soup.select(pos4AndBl4Css)
            pos4 = pos4AndBl4Soup[0].select("p > span")[0].text
            bl4 = pos4AndBl4Soup[0].select("p > i")[0].text
        if checkIfPos5AndBl5Exist:
            pos5AndBl5Soup = soup.select(pos5AndBl5Css)
            pos5 = pos5AndBl5Soup[0].select("p > span")[0].text
            bl5 = pos5AndBl5Soup[0].select("p > i")[0].text
        if checkIfPos6AndBl6Exist:
            pos6AndBl6Soup = soup.select(pos6AndBl6Css)
            pos6 = pos6AndBl6Soup[0].select("p > span")[0].text
            bl6 = pos6AndBl6Soup[0].select("p > i")[0].text

        horseFinTimeCss = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(10)"
        horseFinTime = soup.select(horseFinTimeCss)[0].text

        horseSec1, horseSec2, horseSec3, horseSec4, horseSec5, horseSec6 = None, None, None, None, None, None
        if checkIfPos1AndBl1Exist:
            horseSec1Css = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(4) > p:nth-child(2)"
            horseSec1 = soup.select(horseSec1Css)[0].text
        if checkIfPos2AndBl2Exist:
            horseSec2Css = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(5) > p:nth-child(2)"
            horseSec2 = soup.select(horseSec2Css)[0].text
        if checkIfPos3AndBl3Exist:
            horseSec3Css = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(6) > p:nth-child(2)"
            horseSec3 = soup.select(horseSec3Css)[0].text
        if checkIfPos4AndBl4Exist:
            horseSec4Css = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(7) > p:nth-child(2)"
            horseSec4 = soup.select(horseSec4Css)[0].text
        if checkIfPos5AndBl5Exist:
            horseSec5Css = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(8) > p:nth-child(2)"
            horseSec5 = soup.select(horseSec5Css)[0].text
        if checkIfPos6AndBl6Exist:
            horseSec6Css = RECORDS_ROW_CSS_SELECTOR + f":nth-child({i + 1}) > td:nth-child(9) > p:nth-child(2)"
            horseSec6 = soup.select(horseSec6Css)[0].text
        insertSectionalTimeAndPositionRecords(raceId, finishingOrder, horseNum, horseName, horseId, pos1, bl1, pos2,
                                              bl2, pos3, bl3, pos4, bl4, pos5, bl5, pos6, bl6, horseSec1, horseSec2,
                                              horseSec3, horseSec4, horseSec5, horseSec6, horseFinTime)


def scrapSectionalTimeAndPosition(driver, raceId, sectionalTimeUrl):
    driver.get(sectionalTimeUrl)
    seleniumUtil.checkLoading(driver, RACE_TIME_CSS_SELECTOR)
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    scrapSectionalTimeAndPositionRace(raceId, soup)
    scrapSectionalTimeAndPositionRecords(raceId, soup)


def main():
    allRaceUrl = queryAllRaceDayUrlForSectionalTimeUrl()
    options, chrome_driver_path = webDriverUtil.webDriverSetup()
    driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
    for raceTuple in allRaceUrl:
        raceId = raceTuple[0]
        raceUrl = raceTuple[1]
        sectionalTimeUrl = sectionTimeUrlUtil.parseRaceUrlAndGenerateSectionalTimeUrl(raceUrl)
        updateDatabaseForSectionalTimeLink(raceId, sectionalTimeUrl)
        scrapSectionalTimeAndPosition(driver, raceId, sectionalTimeUrl)
