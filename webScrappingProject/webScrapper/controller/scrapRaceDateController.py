import datetime

from bs4 import BeautifulSoup
from django.db import connection
from selenium import webdriver

from ..utils import seleniumUtil, webDriverUtil
from ..models import RaceCalender, RaceMonthYear

cur = connection.cursor()
initUrl = 'https://racing.hkjc.com/racing/information/English/Racing/Fixture.aspx'

def scrapInsideWebsite(dri, soup):
    monthAndYear = soup.select("body > div > div.fixture_tab > table > thead > tr.bg_blue.color_w > td")[0].text
    if dri.current_url == initUrl:
        monthAndYear = monthAndYear.split("/")
        url = f'https://racing.hkjc.com/racing/information/English/Racing/Fixture.aspx?CalYear={monthAndYear[1]}&CalMonth={monthAndYear[0]}'
        monthAndYear = "/".join(monthAndYear)
    else:
        url = dri.current_url
    calenderDate = soup.select(
        "body > div > div.fixture_tab > table > tbody > tr > td.calendar > p.f_clear > span.f_fl.f_fs14")
    monthAndYearDate = datetime.datetime.strptime(monthAndYear, '%m/%Y').date()
    if not RaceMonthYear.objects.filter(race_month_and_year=monthAndYearDate).exists():
        RaceMonthYear(race_month_and_year=monthAndYearDate, url=url).save()
    for day in calenderDate:
        dayMonthAndYear = f"{day.text}/" + monthAndYear
        date = datetime.datetime.strptime(dayMonthAndYear, '%d/%m/%Y').date()
        if not RaceCalender.objects.filter(race_date=date).exists():
            RaceCalender(race_date=date, race_month_and_year=monthAndYearDate).save()

def startScrap():
    options, chrome_driver_path = webDriverUtil.webDriverSetup()
    driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
    driver.get(initUrl)
    seleniumUtil.checkLoading(driver)
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    monthAndYear = soup.select("body > div > div.fixture_tab > table > thead > tr.bg_blue.color_w > td")[0].text
    monthAndYear = monthAndYear.split("/")
    url = f'https://racing.hkjc.com/racing/information/English/Racing/Fixture.aspx?CalYear={monthAndYear[1]}&CalMonth={monthAndYear[0]}'
    if not RaceMonthYear.objects.filter(url=url).exists():
        scrapInsideWebsite(driver, soup)
    allMonthLink = soup.select("body > div > div.nav > ul > li > a")
    for urlSoup in allMonthLink:
        url = "https://racing.hkjc.com" + urlSoup['href']
        if not RaceMonthYear.objects.filter(url=url).exists():
            driver.get(url)
            seleniumUtil.checkLoading(driver)
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            scrapInsideWebsite(driver, soup)

    driver.quit()
