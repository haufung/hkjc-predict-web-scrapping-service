from django.db import models

class RaceMonthYear(models.Model):
    id = models.AutoField(primary_key=True)
    url = models.TextField(blank=True, null=True, unique=True)
    race_month_and_year = models.DateField(blank=True, null=True)
    objects = models.Manager()

    def __str__(self):
        return self.url

class RaceCalender(models.Model):
    race_date = models.DateField(blank=True, null=True, unique=True)
    race_month_and_year = models.DateField(blank=True, null=True)
    objects = models.Manager()

    def __str__(self):
        return self.race_date