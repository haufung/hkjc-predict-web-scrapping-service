from django.urls import path

from . import views

urlpatterns = [
    path('health/', views.healthCheck, name='health'),
    path('autoUpdate/', views.autoUpdate, name='autoUpdate'),
    path('predictRealTimeRace/', views.predictRealTimeRace, name='predictRealTimeRace'),
    path('scrapRealTimeRace/', views.scrapRealTimeRace, name='scrapRealTimeRace'),
    path('scrapRaceDate/', views.scrapRaceDate, name='scrapRaceDate'),
    path('trainModel/', views.trainModel, name='trainModel'),
    path('calculatedField/', views.calculatedField, name='calculatedField'),
    path('balanceLabel/', views.balanceLabel, name='balanceLabel'),
    path('calculateProfitWithThreshold/', views.calculateProfitWithThreshold, name='calculateProfitWithThreshold'),
    path('updateRaceChineseName/', views.updateRaceChineseName, name='updateRaceChineseName'),
    path('updateSectionalTimeUrl/', views.updateSectionalTimeUrl, name='updateSectionalTimeUrl'),
    path('generateRaceLinkFromDate/', views.generateRaceLinkFromDate, name='generateRaceLinkFromDate')
]