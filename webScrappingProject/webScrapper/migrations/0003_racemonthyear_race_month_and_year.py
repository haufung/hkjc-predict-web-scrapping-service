# Generated by Django 3.1.2 on 2020-10-09 07:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webScrapper', '0002_auto_20201009_1545'),
    ]

    operations = [
        migrations.AddField(
            model_name='racemonthyear',
            name='race_month_and_year',
            field=models.DateField(blank=True, null=True),
        ),
    ]
